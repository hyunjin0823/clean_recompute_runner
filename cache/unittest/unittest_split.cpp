#include "cache.h"
#include "gtest/gtest.h"
#include <vector>

namespace {

TEST(cacheTest, DefaultConstructor) {
   	Cache<int, int> cache;

   	EXPECT_EQ(0u, cache.size());
   	EXPECT_EQ(0u, cache.LRU_size());
}

TEST(cacheTest, SimpleReadWriteTest) {
   	Cache<int, int> cache;
	int value;

   	EXPECT_EQ(false, cache.read(1, value));
   	cache.write(1, 100);
   	EXPECT_EQ(true, cache.read(1, value));
   	EXPECT_EQ(100, value);
   	EXPECT_EQ(100, cache.peek_front());
   	EXPECT_EQ(100, cache.peek_back());

   	cache.write(2, 200);
   	cache.write(3, 300);
   	EXPECT_EQ(300, cache.peek_front());
   	EXPECT_EQ(100, cache.peek_back());
   	EXPECT_EQ(3, cache.size());
   	EXPECT_EQ(3, cache.LRU_size());
}

TEST(cacheTest, longReadWriteTest) {
	int N=100;
	Cache<int, int> cache(N);
	for(int i=1; i<N; i++) {
		cache.write(std::move(i), i*1000);
		EXPECT_EQ(i*1000, cache.peek_front());
		EXPECT_EQ(1000, cache.peek_back());
		EXPECT_EQ(i, cache.size());
		EXPECT_EQ(i, cache.LRU_size());
	}

	int value;
	for(int i=1; i<N; i++) {
		EXPECT_EQ(true, cache.read(i, value));
		EXPECT_EQ(i*1000, value);
		EXPECT_EQ(value, cache.peek_front());
		if(i < N-1)
			EXPECT_EQ((i+1)*1000, cache.peek_back());
		else
			EXPECT_EQ(1000, cache.peek_back());
		EXPECT_EQ(N-1, cache.size());
		EXPECT_EQ(N-1, cache.LRU_size());
	}

}
} // namespace
