#include "cache.h"
#include "gtest/gtest.h"
#include <vector>

namespace {

// define a factory function for creating instances of the implementation
template <typename T>
Cache<T, T>* CreateCache();

template<>
Cache<int, int>* CreateCache<int>() {
	return new Cache<int, int>;
}

template<>
Cache<long, long>* CreateCache<long>() {
	return new Cache<long, long>;
}

template<>
Cache<float, float>* CreateCache<float>() {
	return new Cache<float, float>;
}

template<typename T>
T CreateData();

template<> int 	CreateData<int>() { return 10; }
template<> long CreateData<long>() { return 100l; }
template<> float CreateData<float>() { return 100.1f; }

// define a test fixture class template
template <typename T>
class cacheTest: public testing::Test {
protected:
	cacheTest() : cache_(CreateCache<T>()) {
		data = CreateData<T>();
		key = (T)1.1;
	}
	~cacheTest() override { delete cache_; }

	Cache<T, T>* cache_;
	T data;     // for write
	T value;	// for read
	T key;
};

#if GTEST_HAS_TYPED_TEST

using testing::Types;


typedef Types<int, long, float> Implementation;

TYPED_TEST_SUITE(cacheTest, Implementation);

TYPED_TEST(cacheTest, DefaultConstructor) {

   	EXPECT_EQ(0u, this->cache_->size());
   	EXPECT_EQ(0u, this->cache_->LRU_size());
}

TYPED_TEST(cacheTest, SimpleReadWriteTest) {
   	EXPECT_EQ(false, this->cache_->read(this->key, (this->value)));
   	this->cache_->write(std::move(this->key), std::move(this->data));
   	EXPECT_EQ(true, this->cache_->read(this->key, (this->value)));
   	EXPECT_EQ(this->data, this->value);
   	EXPECT_EQ(this->data, this->cache_->peek_front());
   	EXPECT_EQ(this->data, this->cache_->peek_back());

   	this->cache_->write(this->key*2, this->data*2);
   	this->cache_->write(this->key*3, this->data*3);
   	EXPECT_EQ(this->data*3, this->cache_->peek_front());
   	EXPECT_EQ(this->data, 	this->cache_->peek_back());
   	EXPECT_EQ(3, this->cache_->size());
   	EXPECT_EQ(3, this->cache_->LRU_size());
}

TYPED_TEST(cacheTest, longReadWriteTest) {
	int N=1000;
	this->cache_->resize(N);

	//std::cout << this->key << " " << this->data << std::endl;
	for(int i=1; i<N; i++) {
		this->cache_->write(this->key*i, this->data*i);
		EXPECT_EQ(this->data*i, this->cache_->peek_front());
		EXPECT_EQ(this->data, 	this->cache_->peek_back());
		EXPECT_EQ(i, this->cache_->size());
		EXPECT_EQ(i, this->cache_->LRU_size());
	}
	
	for(int i=1; i<N; i++) {
		EXPECT_EQ(true, this->cache_->read(this->key*i, (this->value)));
		EXPECT_EQ(this->data*i, this->value);
		EXPECT_EQ(this->value, this->cache_->peek_front());
		if(i < N-1)
			EXPECT_EQ(this->data*(i+1), this->cache_->peek_back());
		else
			EXPECT_EQ(this->data, this->cache_->peek_back());
		EXPECT_EQ(N-1, this->cache_->size());
		EXPECT_EQ(N-1, this->cache_->LRU_size());
	}

}
#endif
} // namespace
