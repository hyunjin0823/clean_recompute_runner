#include "cache.h"

using namespace std;

int main() {
  Cache<int,int> c;
  cout << "WRITE: ADDR=1, DATA=1" << endl;
  c.write(1, 1);
  cout << c.peek_front() << " " << c.peek_back() << endl;

  cout << "WRITE: ADDR=1, DATA=10" << endl;
  c.write(1, 10);
  cout << c.peek_front() << " " << c.peek_back() << endl;
  
  cout << "WRITE: ADDR=2, DATA=20" << endl;
  c.write(2, 20);
  cout << c.peek_front() << " " << c.peek_back() << endl;
  
  cout << "WRITE: ADDR=3, DATA=30" << endl;
  c.write(3, 30);
  cout << c.peek_front() << " " << c.peek_back() << endl;
  
  cout << "WRITE: ADDR=4, DATA=40" << endl;
  c.write(4, 40);
  cout << c.peek_front() << " " << c.peek_back() << endl;

  int value;
  c.read(2, value);
  cout << "READ: ADDR=2, DATA-> " << value << endl;
  cout << c.peek_front() << " " << c.peek_back() << endl;
  value=-1;
  
  c.read(3, value);
  cout << "READ: ADDR=3, DATA-> " << value << endl;
  cout << c.peek_front() << " " << c.peek_back() << endl;
  value=-1;
  
  c.read(2, value);
  cout << "READ: ADDR=2, DATA-> " << value << endl;
  cout << c.peek_front() << " " << c.peek_back() << endl;
  value=-1;
  
  c.read(4, value);
  cout << "READ: ADDR=4, DATA-> " << value << endl;
  cout << c.peek_front() << " " << c.peek_back() << endl;
  value=-1;
  
  c.read(5, value);
  cout << "READ: ADDR=5, DATA-> " << value << endl;
  cout << c.peek_front() << " " << c.peek_back() << endl;
  return 0;
}
