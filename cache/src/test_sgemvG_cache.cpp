#include <iostream>
#include <algorithm>
#include <omp.h>
#include <mkl.h>
#include <chrono>
#include "mat.h"
#include "time_chrono.h"
#include "sgemv_cacheGroup.h"

int test(int SIZE, int i, bool noIter=false) {
	int M=SIZE<<i;
	int K=SIZE<<(i-1);
	//int K=SIZE<<i;
	int N=1;
	int iter = 1024;
	if(noIter==true)
		iter=1;
	float *W = new float[M*K];
	float *WT = new float[M*K];
	float *x = new float[K*N];
	float *Y0 = new float[M*N];
	float *Y1 = new float[M*N];
	float *Y2 = new float[M*N];

	std::cout << "SIZE: " << M << ", " << K << " ITERS: " << iter << std::endl;
    std::cout << "#Threads = " << mkl_get_max_threads() << " " << mkl_get_dynamic() << std::endl;
	matFill(W, M, K, true);
	matFill(x, K, N, true);
	
	/*
	Time::start();
	for(int i=0; i<iter; i++)
		matMul(Y0, W, x, M, K, N);	// No optimization
	Time::finish();
   	std::cout << "MatMul     , time =\t" << Time::elapsed() << std::endl;
	*/
	Time::start();
	for(int i=0; i<iter; i++)
		matVecMul(Y0, W, x, M, K); 	// mat * vector with OpenMP
	Time::finish();
   	std::cout << "MatVecMul  , time =\t" << Time::elapsed() << std::endl;
	
	mkl_set_num_threads(1);		// sgemv is not thread-safe if M is larger than 256 x 256
	Time::start();
	for(int i=0; i<iter; i++)
		cblas_sgemv(CblasRowMajor, CblasNoTrans, 
				M, K, 1.0f, W, K, 
				x, N, 0.0f, 
				Y1, N);
	Time::finish();
   	std::cout << "sgemv      , time =\t" << Time::elapsed() << std::endl;
	matComp(Y0, Y1, M, N);

	// tranpose W -> CblasColMajor
	mkl_somatcopy('R', 'T', M, K, 1.0, W, K, WT, M);
	Time::start();
	for(int i=0; i<iter; i++)
		cblas_sgemv(CblasColMajor, CblasNoTrans, 
				M, K, 1.0f, WT, M, 
				x, N, 0.0f, 
				Y1, N);
	Time::finish();
   	std::cout << "sgemvCol   , time =\t" << Time::elapsed() << std::endl;
	matComp(Y0, Y1, M, N);

	// The half bottom nodes are cached
	for(int gSize=1; gSize<K; gSize=gSize<<1) {	// groupSize : 1 to K/2
		vector<pair<int, int>> cacheIdx;
		for(int i=K/2; i<K; i+=gSize) {
			// cacheIdx (std::pair) consists of (the starting index, the size) of cached node 
			cacheIdx.push_back(std::make_pair(i, gSize));
		}
		SgemvCacheG<float> func(W, x, Y2, M, N, K, cacheIdx);
		func.mkl_sgemv_cache(); 	// process cache cold miss
		Time::start();
		for(int i=0; i<iter; i++)
			func.mkl_sgemv_cache();
		Time::finish();
   		std::cout << "sgemvCache , time =\t" << Time::elapsed() << "\t";
		std::cout << "GroupSize = " << gSize << std::endl;
		matComp(Y0, Y2, M, N);
	}
	/*
	vector<pair<int, int>> overlappedIdx;
    overlappedIdx.push_back(std::make_pair(1, 2));
    overlappedIdx.push_back(std::make_pair(2, 1));
	SgemvCacheG<float> func(W, x, Y2, M, N, K, overlappedIdx);
	*/
	delete []W;
	delete []x;
	delete []Y0;
	delete []Y1;
	delete []Y2;
}

int main() {
	for(int i=1; i<6; i++) {
		test(64, i, false);
	}

	return 0;
}
