#include <iostream>
#include <algorithm>
#include <omp.h>
#include <mkl.h>
#include <chrono>
#include "mat.h"
#include "time_chrono.h"

int test(int SIZE, int i, bool noIter=false) {
	int M=SIZE*i;
	int K=SIZE*i;
	int N=SIZE*i;
	int iter = 512>>i;
	if(noIter==true)
		iter=1;
	float *W = new float[M*K];
	float *WT = new float[M*K];
	float *x = new float[K*N];
	float *Y0 = new float[M*N];
	float *Y1 = new float[M*N];

	std::cout << "SIZE: " << M << ", " << K << " ITERS: " << iter << " ----" << std::endl;
    std::cout << "#Threads = " << mkl_get_max_threads() << " " << mkl_get_dynamic() << std::endl;
	matFill(W, M, K, true);
	matFill(x, K, N, true);
	
	Time::start();
	for(int i=0; i<iter; i++)
		matMul(Y0, W, x, M, K, N);
	Time::finish();
   	std::cout << "MatMul     , time =\t" << Time::elapsed() << std::endl;
	
    mkl_set_num_threads_local(1);	// using default got error if SIZE > 256
									// also, there is no benefit having many threads in performance
	Time::start();
	for(int i=0; i<iter; i++)
		cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 
				M, N, K, 1.0f, W, K, 
				x, N, 0.0f, 
				Y1, N);
	Time::finish();
   	std::cout << "sgemm      , time =\t" << Time::elapsed() << std::endl;
	matComp(Y0, Y1, M, N);
	delete []W;
	delete []x;
	delete []Y0;
	delete []Y1;
}

int main() {
	for(int i=1; i<10; i++) {
		test(128, i, false);
	}
	return 0;
}
