#include "utility.h"
#include <numeric>
#include <iostream>

using namespace std;

template <typename T>
void print_vec(vector<T> vec) {
	for(auto v : vec) 
		cout << v << " ";
	cout << endl;
}


template <typename T>
void print_pair(vector<pair<T, T>> vec) {
	for(auto v : vec) 
		cout << "( " << v.first << ", " << v.second << " )\t";
	cout << endl;
}

vector<vector<int>> gen_power_set(int n) {
	vector<vector<int>> power_set;
	for(int i=0; i< (1<<n); i++) { 	// 1<<n = 2^n -> # power sets
		vector<int> power;
		for(int j=0; j<n; j++) {
			if(i & (1 <<j) ) // get the proper element based on i value (bit value)
				power.push_back(j); 
		}
		power_set.push_back(power);
	}
	return power_set;
}

int main() {

 //   for(int size=0;  size<5; size++) {
    for(int size=5;  size<8; size++) {
		cout << "================" << endl;
		cout << "SIZE = " << size << endl;
		cout << "----------------" << endl;
	vector<vector<int>> delimiters = gen_power_set(size);
	for(auto v : delimiters) {
		auto blocks = split(size, v);
		cout << "Delimiter: " ; print_vec<int>(v);
		cout << "Blocks: " ; print_pair<int>(blocks);
		cout << endl;
	}
	}

  	return 0;
}
