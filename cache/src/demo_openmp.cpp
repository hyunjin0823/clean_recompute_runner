#include <iostream>
#include <algorithm>
#include <omp.h>
#include <mkl.h>
#include <chrono>
#include "mat.h"
#include "time_chrono.h"

const int SIZE=4096;
const int VEC_SIZE = SIZE * SIZE;

void test(float *A, float *B, float *C, float *C1) {
	long long testLength = SIZE;
    long long iter = 32*SIZE;

	for(int j=1; j<=SIZE; j*=2, testLength*=2, iter/=2) {
		std::cout << "Length = " << testLength << "\tIterations = " << iter << std::endl;
		Time::start();
		for(int i=0; i<iter; i++)
			VVMul(C, A, B, testLength);
		Time::finish();
		long long ori_time=Time::elapsed();
   		std::cout << "VVMul\t\t, time =\t" << ori_time << std::endl;
	
		Time::start();
		for(int i=0; i<iter; i++)
			VVMulOmp(C1, A, B, testLength);
		Time::finish();
		long long omp_time=Time::elapsed();
   		std::cout << "VVMulOmp\t, time =\t" << omp_time << "\t-> " 
					<< float(omp_time)/ori_time << std::endl;
		vecComp(C, C1, testLength);
	}
}

int main() {
	float *A = new float[VEC_SIZE];
	float *B = new float[VEC_SIZE];
	float *C = new float[VEC_SIZE];
	float *C1 = new float[VEC_SIZE];

	matFill(A, VEC_SIZE, 1);
	matFill(B, VEC_SIZE, 1);
	VVMul(C, A, B, VEC_SIZE); 	// warm-up

	test(A, B, C, C1);
	
	delete []A;
	delete []B;
	delete []C;
	delete []C1;
	return 0;
}
