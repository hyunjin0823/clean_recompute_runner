#include "cache.h"
#include <vector>
#include <memory>

using namespace std;

template<typename T>
void print_vec(const std::vector<T>& vec) {
	for(auto v : vec) {
		std::cout << v << " ";
	}
}

// Test for vector KEY and vector VALUE
int main() {
  	Cache<vector<int>,vector<int>> c;

  	vector<int> key;
 	vector<int> value;
  	for(int i=0; i<4; i++) {
	  	key.push_back(i);
	  	value.push_back(i*100);
		vector<int> temp(value);
	  	c.write(std::move(key), std::move(temp));
		cout << "WRITE: KEY= "; print_vec(key); cout << " VALUE = ";print_vec(value); cout << endl;
  	  	cout << "peek front\t" ; print_vec(c.peek_front());	cout << endl;
  		cout << "peek back\t" ; print_vec(c.peek_back());  	cout << endl;
	}

	key.clear();
	for(int i=0; i<4; i++) {
		key.push_back(i);
		cout << "READ: KEY = "; print_vec(key); cout << endl;
		vector<int> value;
		c.read(key, value);
		print_vec(value);
		cout << endl;
	}

  return 0;
}
