#include <iostream>
#include <algorithm>
#include <omp.h>
#include <mkl.h>
#include <chrono>
#include "mat.h"

using namespace std;

int main() {
	const int SIZE=5;
	const int M=SIZE;
	const int K=SIZE-1;
	float *m = new float[M*K];
	float *mT= new float[M*K];
    
	matFill(m, M, K);
	cout << "Input" << endl;
	matPrint(m, M, K);
	mkl_somatcopy('R', 'T', M, K, 1.0, m, K, mT, M);  
	cout << "Tranpose" << endl;
	matPrint(mT, K, M);
	cout << "Access Input Again" << endl;
	matPrint(m, M, K);
	
	matFill(m, M, K, true);
	cout << "Input" << endl;
	matPrint(m, M, K);
	mkl_somatcopy('R', 'T', M, K, 1.0, m, K, mT, M);  
	cout << "Tranpose" << endl;
	matPrint(mT, K, M);
	cout << "Access Input Again" << endl;
	matPrint(m, M, K);
	return 0;
}
