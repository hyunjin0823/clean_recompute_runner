# SGEMV CACHE
- SgemvCacheG performs the matrix-vector multiplication using a BLAS sgemv function (INTEL MKL cblas_sgemv) with caching.
- A element of the vector or elements (cache grouping) of the vector can have a cache stroage to store previous multiplication results.
Thus, if high locality exists in the vector (e.g., repeative vector inputs), then the corresponding cache hit element(s) multiplication skipped.

## Build
### build release version 
```sh
$ mkdir Release
$ cd Release
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```

### build debug version
```sh
$ mkdir Debug
$ cd Debug
$ cmake -DCMAKE_BUILD_TYPE=Debug ..
$ make
```

## File Description
### include
- cache.h: Define "Cache" class having a key and a value. The key can be built-in type (int, long, float, and doouble) or a vector. The value is a vector that corresponds to the output of the partial matrix-vector multiplication.
- cached_node.h: A class "CachedNode" constructs a hash with a int index to the "Cache" object. The int index corresponds the element index of the vector in the matrix-vector multiplication.
- sgemv_cacheGroup.h: Define SgemvCacheG class that performs the matrix-vector multiplication with caching. SgemvCacheG takes the similar arguments to "cblas_sgemv" in addition to the cache indices information. the cache index consists of the starting cached node index and the size. Check the src/test_sgemvG_cache.cpp for the detail.
- mat.h: define matrix related functions
- utility.h: define split function used in "SgemvCacheG" class
- time_chrono.h: simple time class for time measurement
 
### src
- test_sgemvG_cache.cpp: simple test program for various sgemv functions including "sgemvCacheG"
- demo_*.cpp: various demo (test) program

### unittest
- unittest_cache1.cpp: google test for "Cache" class
- unittest_cache2.cpp: second google test for "Cache" class
- unittest_split.cpp: google test for "split" function

## SgemvCacheG Explanation
### 1. Column-Major-Order
SgemvCacheG takes a row-major-order matrix, then keeps its tranposed one internally when it is constructed. The reason of the tranposition is to easily cut out the columns of the original matrix. Moreover, cblas_sgemv performs better with column-major-order matrix (tranposed one). Thus, SgemvCacheG uses the column-major-order matrix when it uses cblas_sgemv function.

### 2. Cache Index
"cache_idx_" variable in SgemvCacheG keeps the information of the vector element having a cache. It is a std::pair such as (the starting index, the size). If the size is larger than one, it is considered as a cache block (or cache group). The cache block covers multiple elements at onece. Bigger size block reduces the caching overhead as well as the cache hit rate.
