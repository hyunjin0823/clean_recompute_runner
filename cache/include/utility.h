#pragma once
#include <vector> 
#include <utility> // for pair

// split the n is two parts except for the delimiter is 0 or n-1
std::vector<std::pair<int, int>> split(int n, std::vector<int>& delimiter) {
    std::vector<std::pair<int, int>> partial_indices;
	int prev_start=-1, prev_size=0;
	for(auto d : delimiter) {
		//assert(d<n-1);
		//std::cout << "d = " << d << std::endl;
		if(prev_start+1 < d) {	// consecutive hits
			partial_indices.push_back(std::make_pair(prev_start+1, d-prev_start-1));
		}
		prev_start=d;
	}
	if(prev_start<n-1) // add the last block
		partial_indices.push_back(std::make_pair(prev_start+1, n-prev_start-1));
	return partial_indices;
}
