#pragma once
#include <chrono>

class Time {
	static std::chrono::time_point<std::chrono::high_resolution_clock> begin;
	static long long delta;
public:
	static void start() {
		Time::begin = std::chrono::high_resolution_clock::now();
	}
	static void finish() {
		auto measured = std::chrono::high_resolution_clock::now() - Time::begin;
		Time::delta = std::chrono::duration_cast<std::chrono::microseconds>(measured).count();
	}

	static long long elapsed() {
		return Time::delta;
	}
};

std::chrono::time_point<std::chrono::high_resolution_clock> Time::begin;
long long Time::delta;