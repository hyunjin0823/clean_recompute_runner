#ifndef CACHE_H_
#define CACHE_H_
#include <iostream>
#include <cassert>
#include <list>
#include <unordered_map>
#include <utility>
#include <memory>
#include <vector>
#include <type_traits>  // is_fundamental
#include <functional>	// for hash
#include <boost/functional/hash.hpp>

// For cache grouping that uses vector as a Key
template <typename Container>
struct container_hash {
	std::size_t operator()(Container const &c) const {
		return boost::hash_range(c.begin(), c.end());
	}
};

template <>
struct container_hash<int> {
	std::size_t operator()(int const &c) const {
		return std::hash<int>()(c);
	}
};

template <>
struct container_hash<long> {
	std::size_t operator()(long const &c) const {
		return std::hash<long>()(c);
	}
};

template <>
struct container_hash<float> {
	std::size_t operator()(float const &c) const {
		return std::hash<float>()(c);
	}
};

template <>
struct container_hash<double> {
	std::size_t operator()(double const &c) const {
		return std::hash<double>()(c);
	}
};

template <typename KEY, typename VALUE>
class Cache {
private:
	typedef std::unordered_map<KEY, std::pair<VALUE, typename std::list<KEY>::iterator>, //> Table; 
		container_hash<KEY> > Table;
  	Table m_cache;
	std::list<KEY> LRU;
	size_t m_capacity;

  	// move the most recent accessed item to the front.
  	void updateLRU(KEY key, const typename Table::iterator& it) {
		// 1. remove the target entry in the ordering list 
    	LRU.erase(it->second.second);	// erase with iterator
		// 2. add the removed one to the front
    	LRU.emplace_front(key);
		// 3. update the reference
    	it->second.second = LRU.begin();
  	}

	// remove the least recenltly used buffer
	void evictLRU() {
		m_cache.erase(LRU.back());
		LRU.pop_back();
	}

public:
	Cache(size_t capacity=4): m_capacity(capacity) 
	{
		 // set the size of hash memory
		 m_cache.reserve(m_capacity);
	}
	virtual ~Cache() {};

	bool resize(size_t size) {
		if(size > m_capacity) {
			m_capacity = size;
			m_cache.reserve(m_capacity);
			return true;
		} else
			return false;
	}
  	
	// try to read buffered value before matmul
	virtual bool read(KEY key, VALUE &value) {
    	auto it = m_cache.find(key);	// access cache
    	if (it == m_cache.end()) {      // cache misses
      		return false;		
    	}
		// read the cache block
    	value = it->second.first;
        // make the read buffer as the most recenly used buffer
		updateLRU(key, it);
    	return true;       	// cache read hit
  	}

	// write the value after matmul on a cache miss
  	virtual void write(KEY key, VALUE &&value) { 	// take the rvalue referece argument to avoid copy
    	auto it = m_cache.find(key);
		// if cache hits
		// assert(it == m_cache.end());	// a write always misses the cache
    	if (it != m_cache.end()) {
			// update data ?, it cannot occur 
        	it->second.first = value;
      		updateLRU(key, it);
    	} else { // cache misses
      		if (m_cache.size() == m_capacity) {	// if cache is full
				evictLRU();	// remove the LRU buffer
      		}

	  		// fill the data into cache 
      		LRU.emplace_front(key);		// put the new data iter on the front of LRU list
      		m_cache[key] = {value, LRU.begin()};	// fill new data in the cache
    	}
	}

	// for the test
	VALUE peek_front() 	{ assert(m_cache.size()>0); return m_cache[LRU.front()].first; }
	VALUE peek_back() 	{ assert(m_cache.size()>0); return m_cache[LRU.back()].first; }
	size_t size() { return m_cache.size(); }
	size_t LRU_size() { return LRU.size(); }
};
#endif
