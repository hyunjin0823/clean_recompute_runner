#pragma once
#include "cache.h"
#include <memory>
#include <numeric>
#include <algorithm> // for generate
#include <omp.h>
#include <mat.h>

using std::vector;

// FC_Cache implementation
template <typename Key, typename Value> 
class FC_Cache {
private:
	//size_t cache_size_;
	std::unordered_map<int, std::shared_ptr<Cache<Key, Value>>> cache_;
public:
	// the vector "cached" has node indices which has a cache
	FC_Cache(const std::vector<int> &cached, size_t cache_size=4)
	//: cache_size_(cache_size) { 
	{
		for(auto i : cached) 
			cache_[i] = std::make_shared<Cache<Key, Value>>();
	}
	
	FC_Cache(size_t n, size_t cache_size=4)
	//: cache_size_(cache_size) { 
	{
		//for(size_t i=0; i<n; i++)
		//	cache_[i] = std::make_shared<Cache<Key, Value>>();
		std::generate(cache_.begin(), cache_.load_factorend(), 
				      std::make_shared<Cache<Key, Value>>());
	}

	virtual ~FC_Cache() {}

	// check the node has a cache
	bool has_cache(int idx) {
		if(cache_.find(idx) == cache_.end()) // not found
			return false;
		else
			return true;
	}

	// return a bit vector for nodes having cache
	std::vector<bool> cache_list(size_t n) {
		std::vector<bool> cache_idx;
		for(size_t i=0; i<n; i++) {
			cache_idx.push_back(has_cache(i));
		}
		return cache_idx;
	}

	bool read_cache(int idx, Key key, Value& cached_data) {
		if(has_cache(idx)==false)
			return false;
		return cache_[idx]->read(key, cached_data); 
	}

	bool write_cache(int idx, Key key, Value&& new_data) {
		if(has_cache(idx)==false)
			return false;
		cache_[idx]->write(key, std::move(new_data));
		return true;
	}
///*
	void process_caching(Key* x, vector<int>& hits, vector<int>& misses, 
						 std::vector<Value>& data) {
		for(auto i : cache_) {
			Value value;
			if(read_cache(i, x[i], &value)) { 	// cache hit
				hits.push_back(i);
				data.push_back(value);
			} else { 							// cache miss
				misses.push_back(i);
			}
		}
	}

	void update_cache(Key* x, vector<int>& misses, std::vector<Value>& new_data) {
		for(int i=0; i<misses.size(); i++) {
			int idx = misses[i];
			write_cache(idx, x[idx], new_data[i]);
		}
	}
//*/		
};

template <typename Key, typename Value> 
class FC_CacheCtrl {
public:
	FC_Cache<Key, Value> cache_;
	int size_;
	vector<int> cached_idx_; 		// cache index
	vector<int> cached_hit_;  		// cache hit index
	vector<int> cached_miss_;   	// cache write index
	vector<Value> read_data;       	// list of cached data from cache hits
	//vector<Value> write_data;     // list of new data to write to the cache

public:
	FC_CacheCtrl(const vector<int> &cached, size_t cache_size=4): 
		cache_(cached, cache_size), cached_idx_(cached), size_(-1) {}		

	FC_CacheCtrl(size_t n, size_t cache_size=4): cache_(n, cache_size), size_(n) {}
		
	virtual ~FC_CacheCtrl() {}

	void process_caching(Key* x) {
		cached_hit_.clear();
		cached_miss_.clear();
		read_data.clear();
		for(auto i : cached_idx_) {
			Value value;
			if(cache_.read_cache(i, x[i], value)) { 	// cache hit
				cached_hit_.push_back(i);
				read_data.push_back(value);
			} else { 									// cache miss
				cached_miss_.push_back(i);
			}
		}
	}
	
	void process_caching(Key* result, Key* x) {
		cached_hit_.clear();
		cached_miss_.clear();
		read_data.clear();
		for(auto i : cached_idx_) {
			Value value;
			if(cache_.read_cache(i, x[i], value)) { 	// cache hit
				cached_hit_.push_back(i);
				for(int j=0; j<value.size(); j++) {  	// handle cache hit here
				   result[j]+=value[j];
				}
			} else { 									// cache miss
				cached_miss_.push_back(i);
			}
		}
	}

	void update_caching(Key* x, vector<Value> &new_data) {
		int idx=0;
		for(auto i : cached_miss_) {
			cache_.write_cache(i, x[i], std::move(new_data[idx++]));
		}
		assert(idx == cached_miss_.size());
		assert(idx == new_data.size());
	}

	size_t size() 		{ return cached_idx_.size(); }
	size_t size_hit() 	{ return cached_hit_.size(); }
	size_t size_miss() 	{ return cached_miss_.size(); }
};
