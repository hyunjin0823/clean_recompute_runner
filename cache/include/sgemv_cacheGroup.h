#pragma once
#include "cached_node.h"
#include "mat.h"
#include "utility.h"	// split function
#include <omp.h>
#include <mkl.h>
#include <utility> // for pair

#include <iostream>
using namespace std;

using std::vector;
using std::pair;

// *a = W, *b = x, *c = Y
template <typename T>
class SgemvCacheG{
private:
	CachedNode<vector<T>, vector<T>> cachedNode_;
	T *weight_T;
	T *weight_;
	T *input_;
	T *result_;
	int M_, N_, K_;
	int groupSize_;

	vector<pair<int, int>> cached_idx_;
	vector<pair<int, int>> cache_hit_;
	vector<pair<int, int>> cache_miss_;
	vector<pair<int, int>> uncached_blocks;

	vector<T> genKeys(pair<int, int> idx) {
		vector<T> keys;
		for(int key= idx.first; key<idx.first+idx.second; key++) {
	   		keys.push_back(input_[key]);
	   	}
		return keys;
	}

	// Add the cached partial result
	void add_cached(const vector<T> &value) {
		for(int i=0; i<M_; i++) 
			result_[i] += value[i];
	}
	
	// Perform a partial sgemv on cache missed node and update the cache
	void cache_miss_handle() {
		// handle cache misses
		T *temp = new T[M_];
		for(auto idx : cache_miss_) {
			vector<T> t_vec(M_);
			// Calcualte cache missed one
			cblas_sgemv(CblasColMajor, CblasNoTrans, M_, idx.second, 1.0f,
						weight_T +(idx.first*M_), M_, 
						input_ + idx.first, N_, 0.0f,
						temp, N_);	
			for(int i=0; i<M_; i++) {
				result_[i]+=temp[i];
				t_vec[i]=temp[i];
			}
            vector<T> keys = genKeys(idx);
			cachedNode_.write_cache(idx.first, std::move(keys), std::move(t_vec));
		}
		delete []temp;
	}

	// Get the uncached blocks' indices from cached node indices
	void get_uncached_blocks() {
		vector<int> cached_indices;
		// get cached node indices
		for(auto startIdx : cached_idx_) {
			for(int i=startIdx.first; i<startIdx.first+startIdx.second; i++)
				cached_indices.push_back(i);
		}
		uncached_blocks = split(K_, cached_indices);
	}

	void process_uncached_block() {
		bool first = true;
		for(auto block : uncached_blocks) { 	
			if(first) {
				cblas_sgemv(CblasColMajor, CblasNoTrans, M_, block.second, 1.0f,
						weight_T+(block.first*M_), M_, 
						input_+(block.first), N_, 0.0f,
						result_, N_);
				first = false;
			} else {
				// If there are more than one uncached blocks,
				// partial sgemv results must be added to the previous one
				cblas_sgemv(CblasColMajor, CblasNoTrans, M_, block.second, 1.0f,
						weight_T+(block.first*M_), M_, 
						input_+(block.first), N_, 1.0f,
						result_, N_);
			}
		}
	}

	// figure out cache hit or misses
	void access_cache() {
		cache_hit_.clear();
		cache_miss_.clear();
		for(auto idx : cached_idx_) {
			vector<T> value;
			vector<T> keys = genKeys(idx);
			if(cachedNode_.read_cache(idx.first, keys, value)) {	// cache hit
				cache_hit_.push_back(idx);
				add_cached(value);									// added cached value
			} else { 												// cache miss
				cache_miss_.push_back(idx);
			}
		}
	}

public:
	SgemvCacheG(T *weight, T *input, T *result,
		const int M, const int N, const int K, 
		vector<pair<int, int>> cache_idx, size_t cache_size=4, int groupSize=1) 
	: cachedNode_(cache_idx, cache_size), cached_idx_(cache_idx), 
		M_(M), N_(N), K_(K), groupSize_(groupSize) {
		weight_ = weight;
		input_ 	= input;
		result_ = result;
		weight_T = new float[M*K];
		
		// tranpose weight in weight Transpose
		mkl_somatcopy('R', 'T', M, K, 1.0, weight, K, weight_T, M);

		// get the indices of uncached blocks
		get_uncached_blocks();
	}

	~SgemvCacheG() { delete [] weight_T; }

	/* 1. perforam partial sgemv excluding cached node
	   2. access cache and add cached result to the partial result
	   3. cache miss handling 
	   	  a. calculate the missed node's sgemv
		  b. add the missed node's result to the partial result
		  c. update the cache with the missed node's result
	*/
	void mkl_sgemv_cache(float *input=nullptr) {
		if(input != nullptr)
			input_ = input;
		
	   	if(cached_idx_.size()==0)	// no Cache
		{
		   	cblas_sgemv(CblasColMajor, CblasNoTrans, M_, K_, 1.0f,
		   			weight_T, M_, 
		   			input_, N_, 0.0f,
		   			result_, N_);
		} else {
			process_uncached_block();	// 1
	   		access_cache();				// 2
			if(cache_miss_.size() > 0) {
	   			cache_miss_handle();    // 3
			}
		}
	}
};
