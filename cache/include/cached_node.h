#pragma once
#include "cache.h"
#include <memory>
#include <numeric>
#include <algorithm> // for generate
#include <omp.h>
#include <mat.h>
#include <utility> // for std::pair
#include <set>

using std::vector;
using std::pair;

// CachedNode implementation
template <typename Key, typename Value> 
class CachedNode {
private:
	//size_t cache_size_;
	// the key of cache_ is the cache input node index
	std::unordered_map<int, std::shared_ptr<Cache<Key, Value>>> cache_;
public:
	// the vector "cached" has node indices which has a cache
	CachedNode(const std::vector<int> &cachedInfo, size_t cache_size=4)
	//: cache_size_(cache_size) { 
	{
		for(auto i : cachedInfo) 
			cache_[i] = std::make_shared<Cache<Key, Value>>();
	}
	
	CachedNode(size_t n, size_t cache_size=4)
	//: cache_size_(cache_size) { 
	{
		//for(size_t i=0; i<n; i++)
		//	cache_[i] = std::make_shared<Cache<Key, Value>>();
		std::generate(cache_.begin(), cache_.load_factorend(), 
				      std::make_shared<Cache<Key, Value>>());
	}
	
	CachedNode(const std::vector<pair<int, int>> &cachedInfo, size_t cache_size=4)
	//: cache_size_(cache_size) { 
	{
		// cachedInfo = { a starting index, group size } 
		std::set<int> overlap;
		for(auto i : cachedInfo) {  
			cache_[i.first] = std::make_shared<Cache<Key, Value>>();
			try{
				for(int idx=i.first; idx<i.first+i.second; idx++) {
					if(overlap.find(idx)!=overlap.end())	// if overlap is found
						throw "Overlapped found in the cached node!";
					else
						overlap.insert(idx);
				}
			} catch (const char *msg) {
				std::cerr << msg << std::endl;
			}
		}
	}

	virtual ~CachedNode() {}
	// check the node has a cache
	bool has_cache(int idx) {
		if(cache_.find(idx) == cache_.end()) // not found
			return false;
		else
			return true;
	}

    /*
	// return a bit vector for nodes having cache
	std::vector<bool> cache_list(size_t n) {
		std::vector<bool> cache_idx;
		for(size_t i=0; i<n; i++) {
			cache_idx.push_back(has_cache(i));
		}
		return cache_idx;
	}
    */
	bool read_cache(int idx, Key key, Value& cached_data) {
		if(has_cache(idx)==false)
			return false;
		return cache_[idx]->read(key, cached_data); 
	}

	bool write_cache(int idx, Key&& key, Value&& new_data) {
		if(has_cache(idx)==false)
			return false;
		cache_[idx]->write(std::move(key), std::move(new_data));
		return true;
	}
};
