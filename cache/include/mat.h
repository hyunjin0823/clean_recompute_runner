#pragma once
#include <iostream>
#include <utility>
#include <vector>
#include <omp.h>
#include <cassert>
#include <random>

void matFill(float *mat, int row, int col, bool random=false, int s=0) {
    std::default_random_engine dre;
	dre.seed(s); 
    std::uniform_int_distribution<int> di(0, 9);
    
	if(random) {
		for(int i=0; i<row; i++) {
			for(int j=0; j<col; j++) {
				mat[i*col +j] = di(dre);
			}
		}
    } else {
		for(int i=0; i<row; i++) {
			for(int j=0; j<col; j++) {
				mat[i*col +j] = (float)((i*col + j)%10); 
			}
		}
	}
}

void matMul(float *C, float *A, float *B, int m, int k, int n) {
	for(int i=0; i<m; i++) {
		for(int j=0; j<n; j++) {
			float sum=0.0f;
			for(int t=0; t<k; t++) {
				sum += A[i*k+t] * B[t*n+j];
			}
			C[i*n + j] = sum;
		}
	}
}

// B or x is a vector size n, MxN matrix, N == 1
void matVecMul(float *C, float *A, float *B, int m, int k) {	
	//#pragma omp parallel for num_threads(4) //schedule(static,32)
	#pragma omp parallel for schedule(static,32) // best	
	for(int i=0; i<m; i++) {
		float sum = 0.0f;	
		int rowIdx = i*k;
		for(int j=0; j<k; j++) {			
			sum += A[rowIdx + j] * B[j];
		}
		C[i] = sum;
	}
	//}
}

// B or x is a vector size n, MxN matrix, N == 1
// A is column major order, No OpenMP since OpenMP slows down
void matTVecMul(float *C, float *A, float *B, int m, int k) {	// use original (before tranposed)  m and k	
	for(int j=0; j<m; j++) {	// for the first value			
			C[j] = A[j] * B[0];
	}

	for(int i=1; i<k; i++) {	// tranposed #rows
		int stride = i*m;
		for(int j=0; j<m; j++) {			
			C[j] += A[stride + j] * B[i];
		}
	}
}

void VVMul(float *C, float *A, float *B, int n) {

	for(int i=0; i<n; i++) 
		C[i] = A[i] * B[i];
}

void VVMulOmp(float *C, float *A, float *B, int n) {
	#pragma omp parallel for //num_threads(2) //schedule(static, 16) 
	for(int i=0; i<n; i++) 
		C[i] = A[i] * B[i];
}

bool vecComp(float *A, float *B, int n) {
	for(int i=0; i<n; i++) {
		if(A[i] != B[i]) {				
			std::cout << "diff: [ " << i << " ], A = " << A[i] << "B = "
						  << "\t" << B[i] << std::endl;
			return false;
		}
	}
	return true;
}

void matPrint(float *mat, int row, int col) {
	for(int i=0; i<row; i++) {
		for(int j=0; j<col; j++) {
			std::cout << mat[i*col + j] << " ";
		}
		std::cout << std::endl;
	}
}

bool matComp(float *A, float *B, int row, int col) {
	bool diff=false;
	for(int i=0; i<row; i++) {
		for(int j=0; j<col; j++) {
			if(A[i*col+j] != B[i*col+j]) 
				std::cout << "diff: [ " << i << " ][ " << j << " ], A = " << A[i*col + j] << "\tB = "
						  << "\t" << B[i*col +j] << std::endl;
				return false;				
		}

	}
	return true;
}
