
CXX= false
CPPFLAGS= 
INTELLIB= /opt/intel/lib/intel64
MKLROOT= /opt/intel/mkl

$(shell mkdir -p bin)

#USECUDA= true
#USECUDA= false
CUDAFLAGS= --default-stream per-thread -arch=sm_61
#CUDAIFLAGS= -I/usr/local/cuda-8.0/include
CUDAIFLAGS= -I/usr/local/cuda/include
ifeq ($(USECUDA),true)
CUDALFLAGS= -lcudart -L/usr/local/cuda/lib64 -lcuda
endif

CUDASRCS= $(wildcard src/ops/op_*.cu)
CUDAOBJS= $(patsubst src/ops/%.cu, bin/cuda/%.o, $(CUDASRCS))

CLEAN_CMD= rm -f bin/*.o bin/run_graph bin/bench_scalability bin/cuda/*.o bin/bench_performance bin/test_correctness

icc-opt: CXX= icpc
icc-opt: CPPFLAGS= -O3 -march=native -DNDEBUG
icc-opt: icc

icc-dbg: CXX= icpc
icc-dbg: CPPFLAGS= -g -traceback -debug all #-D_GLIBCXX_DEBUG
icc-dbg: icc

gcc-opt: CXX= g++
gcc-opt: CPPFLAGS= -Wall -Werror=return-type -Wno-reorder -Wno-sign-compare -Wno-narrowing -O3 -DNDEBUG
gcc-opt: gcc

gcc-dbg: CXX= g++
gcc-dbg: CPPFLAGS= -Wall -Werror=return-type -Wno-reorder -Wno-sign-compare -Wno-narrowing -g -rdynamic -ggdb -fstack-protector-all
gcc-dbg: gcc

gcc: GENFLAGS= -std=c++14 -fopenmp
gcc: MKLIFLAGS= -m64 -I${MKLROOT}/include
gcc: MKLLFLAGS= -m64 -L${MKLROOT}/lib/intel64 -L${INTELLIB} -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl
gcc: all

CUDACOMPILECPPFLAGS= -Wall -Werror=return-type -Wno-reorder -Wno-sign-compare -Wno-narrowing -O3 -DNDEBUG -D USE_CUDA
CUDACOMPILEGENFLAGS= -std=c++11 -fopenmp
CUDACOMPILEMKLIFLAGS= -m64 -I${MKLROOT}/include
CUDACOMPILEMKLLFLAGS= -m64 -L${MKLROOT}/lib/intel64 -L${INTELLIB} -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl


flags:
	-@echo $(MAKECMDGOALS) | cmp -s .make_options || $(CLEAN_CMD) && echo $(MAKECMDGOALS) > .make_options

all: flags bin/run_graph bin/bench_scalability bin/bench_performance bin/test_correctness

ifeq ($(USECUDA),true)
bin/cuda/op_%.o: src/ops/op_%.cu
	nvcc -ccbin=g++ --compiler-options='$(CUDACOMPILEFLAGS) $(CUDACOMPILEGENFLAGS) $(CUDACOMPILEMKLIFLAGS)' --linker-options='$(CUDACOMPILEMKLLFLAGS) $(CUDALFLAGS)' $(CUDAFLAGS) -std=c++11 -c $< -o $@ -D USE_CUDA -O3

bin/ops.o: src/ops/ops.cpp src/ops/*.hpp src/ops/*.h src/*.h src/ops/gpu_scheduler.cu
	nvcc -ccbin=$(CXX) --compiler-options='$(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(CUDAIFLAGS) -D USE_CUDA' --linker-options='$(MKLLFLAGS) $(CUDALFLAGS)' $(CUDAFLAGS) -c src/ops/ops.cpp -o bin/ops.o -D USE_CUDA

bin/def.o: src/def.cpp src/*.h src/def.cu
	nvcc -ccbin=$(CXX) --compiler-options='$(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) -D USE_CUDA' --linker-options='$(MKLLFLAGS) $(CUDALFLAGS)' $(CUDAFLAGS) -c $< -o $@ -D USE_CUDA

bin/graph.o: src/graph.cpp src/*.h
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDAIFLAGS) -c $< -o $@ -D USE_CUDA

bin/load.o: src/load.cpp src/*.h
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDAIFLAGS) -c $< -o $@ -D USE_CUDA

bin/run_graph: src/run.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o $(CUDAOBJS)
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/run_graph src/run.cpp bin/*.o $(CUDAOBJS)

bin/bench_scalability: src/scalability.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o $(CUDAOBJS)
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/bench_scalability src/scalability.cpp bin/*.o $(CUDAOBJS)

bin/bench_performance: src/performance.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o $(CUDAOBJS)
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/bench_performance src/performance.cpp bin/*.o $(CUDAOBJS)

bin/test_correctness: src/correctness.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o $(CUDAOBJS)
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/test_correctness src/correctness.cpp bin/*.o $(CUDAOBJS)
else
bin/ops.o: src/ops/ops.cpp src/ops/*.hpp src/ops/*.h src/*.h
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDAIFLAGS) -c src/ops/ops.cpp -o bin/ops.o

bin/def.o: src/def.cpp src/*.h
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDAIFLAGS) -c $< -o $@

bin/graph.o: src/graph.cpp src/*.h
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDAIFLAGS) -c $< -o $@

bin/load.o: src/load.cpp src/*.h
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDAIFLAGS) -c $< -o $@

bin/run_graph: src/run.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/run_graph src/run.cpp bin/*.o

bin/bench_scalability: src/scalability.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/bench_scalability src/scalability.cpp bin/*.o

bin/bench_performance: src/performance.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/bench_performance src/performance.cpp bin/*.o

bin/test_correctness: src/correctness.cpp bin/def.o bin/ops.o bin/graph.o bin/load.o
	$(CXX) $(CPPFLAGS) $(GENFLAGS) $(MKLIFLAGS) $(MKLLFLAGS) $(CUDALFLAGS) $(CUDAIFLAGS) -o bin/test_correctness src/correctness.cpp bin/*.o
endif

clean:
	-@$(CLEAN_CMD)

