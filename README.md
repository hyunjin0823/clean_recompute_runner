## Prerequisites

- Intel MKL Library
- Add Intel MKL Library to library path
```
export LD_LIBRARY_PATH=/opt/intel/mkl/lib/intel64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/opt/intel/lib/intel64:${LD_LIBRARY_PATH}
```


## README
To be clear, many ideas are still experimental, so there are lots of pieces commented out
and many areas are works in progress. For example, we are still working on how to identify
differences in input images to automate the recompute process.

The high level picture is that this runner loads the model, does some warm up runs before
collecting any timing data, and then does a compute base run followed by a recompute run

You will see the timing info given for the original run and recompute run

The input image is already set and is of size 1920 x 1080

For now the recompute window can be set in src/graph.cpp on line 485, but currently is at
x,y position (300,300) and is of size 250 x 250

The code to switch the recompute run to a different image is close, but is
commented out for now

To see the ops I have been modifying, take a look in src/ops
Most of the work has been done in op_conv2d.hpp
op_maxpool.hpp and op_avgpool.hpp have changes too
The normal and recompute code is in each file and some comments are there

Make using the makefile in the root directory:
"make icc-opt"

or

"make icc-dbg"

Run in the root directory using:
numactl --cpubind=1 --membind=1 ./bin/run_graph models/inception/model

or

numactl --cpubind=1 --membind=1 ./bin/run_graph models/resnet50/model


More information on CONV Operator:

  More comments are included in the code to give some explanation of what each chunk of code
  is doing, exact details may still be unclear, but the purpose of each chunk should be
  much easier to understand

  The recompute version of this op involves finding the affected area for the next layer
  so it knows what to compute and then only doing the computation for that portion

  When it comes to computing the op, it is important to remember that the high level
  picture is that conv2D has a general case and 2 special cases:
    The simplest case is if the kernel and input are the same size

    Another easier case is if the kernel is 1x1

    The final case handles everything else

  When it comes to doing the actual computation, the original and recompute versions differ in a few key ways:
    Both will need to unpack the patches for the MatMul
      The original will do this for all patches
      The recompute will only do this for affected patches

    Both will then do a MatMul, the recompute is a subset of the original MatMul for the
    unrolled patches, the kernel matrix is identical

    At this point, the original is done, but the recompute must overwrite the relevant
    partial results. This is done by identifying the output element that each patch
    corresponds to and overwriting that value in the stored intermediate results
