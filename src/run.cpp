#include <iostream>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <mkl.h>

#include "ops/register.h"
#include "graph.h"
#include "load.h"

int main(int argc, char** argv) {
  RegisterOps();
  if(argc != 2) {
    std::cerr << "usage: ./run_graph <graph.def>\n";
    exit(1);
  }

  omp_set_nested(1); 			// enable nested parallel
  omp_set_dynamic(0);         	// disable dynamic adjestment
  omp_set_max_active_levels(2);

  std::cout << "Loading the graph..." << std::endl;

  OpGraph op_graph = load_graph(argv[1]);
  bool first = true;

  std::cout << "Done loading graph. \n \n";
  //Warm up the graph for better timing info
  for (int i = 0; i < 5; i++)
  //for (int i = 0; i < 0; i++)
  {
    op_graph.run_st(first);
    //op_graph.run_mt(first);
    op_graph.reset();
  }

  //Variables for below
  auto t1 = std::chrono::high_resolution_clock::now();
  auto t2 = std::chrono::high_resolution_clock::now();
  unsigned delta = 0;
  //unsigned deltaOrg = 0;
  //unsigned deltaRec = 0;

  std::cout << "Done warming up the graph. \n \n";
  std::cout << "Doing first runs... \n";

  //for (int j = 0; j < 5; j++) {
    //for (int i = 0; i < 10; i++)
    for (int i = 0; i < 1; i++)
    {
      t1 = std::chrono::high_resolution_clock::now();
      op_graph.run_st(first);
      //op_graph.run_mt(first);
      t2 = std::chrono::high_resolution_clock::now();
      delta += std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      //deltaOrg += std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      //op_graph.print_outputs();
      op_graph.reset();
    }

    change_graph_input(op_graph, argv[1], "second");

    //Print out the average time for 100 runs
    //std::cout << delta/10 << " microseconds for first runs \n \n";
    std::cout << delta << " microseconds for first run \n \n";
    //op_graph.print_outputs();

    //Run second time
    first = false;
    delta = 0;
    std::cout << "Doing second runs..." << std::endl;

    //for (int i = 0; i < 10; i++)
    for (int i = 0; i < 1; i++)
    {
      t1 = std::chrono::high_resolution_clock::now();
      op_graph.run_st(first);
      //op_graph.run_mt(first);
      t2 = std::chrono::high_resolution_clock::now();
      delta += std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      //deltaRec += std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      //op_graph.print_outputs();
      op_graph.reset();
    }
    std::cout << delta << " microseconds for second run \n \n";
    //change_graph_input(op_graph, argv[1], "BigWhite");
    //first = true;
  //}

  //std::cout << delta/10 << " microseconds for second runs\n";
  //std::cout << deltaOrg/5 << " microseconds for original runs \n";
  //std::cout << deltaRec/5 << " microseconds for recompute runs \n";
  
  return 0;
}
