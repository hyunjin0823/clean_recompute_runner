#include <iostream>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <mkl.h>

#include "graph.h"
#include "load.h"
#include "ops/register.h"

int main(int argc, char** argv) {
  RegisterOps();
  if(argc != 2) {
    std::cerr << "usage: ./run_graph <graph.def>\n";
    exit(1);
  }

  omp_set_nested(1);
  omp_set_dynamic(0);
  omp_set_max_active_levels(2);
  mkl_set_dynamic(0);

  OpGraph op_graph = load_graph(argv[1]);
  op_graph.bench_scalability(12);

  return 0;
}
