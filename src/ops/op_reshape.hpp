#pragma once
#include <cstring>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

class ReshapeOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    //assert(args.size() == 2);
    assert(args[1].dtype == DT_INT);
    if(args[0].dtype == DT_FLOAT) return { Kernel(*(args[0].t_float), *(args[1].t_int)), args[args.size()-2] };
    else if(args[0].dtype == DT_INT) return { Kernel(*(args[0].t_int), *(args[1].t_int)), args[args.size()-2] };
    else if(args[0].dtype == DT_LONG) return { Kernel(*(args[0].t_long), *(args[1].t_int)), args[args.size()-2] };
    else {
      assert(false);
      return std::vector<TensorContainer>();
    }
  }
  template<typename T>
  TensorContainer Kernel(Tensor<T> &A, Tensor<int> &B) {
    assert(B.dims() == 1);
    std::vector<size_t> shape(B.elements());
    int idx_arbitrary = -1;
    size_t elem = 1;
    for(size_t i = 0; i < B.elements(); i++) {
      int dim = B.ptr()[i];
      if(dim < 0) {
        if(idx_arbitrary < 0) {
          idx_arbitrary = i;
        } else {
          fatal_error("Too many arbitrary dimensions in " + name());
        }
      } else {
        shape[i] = dim;
        elem *= dim;
      }
    }
    if(idx_arbitrary >= 0) {
      shape[idx_arbitrary] = A.elements() / elem;
    }
    TensorContainer out(DTypeMap<T>::type, shape);
    Tensor<T> *t_out = TensorExtractor<T>::Get(out);
    assert(t_out->elements() == A.elements());
    memcpy(t_out->ptrInit(), A.ptr(), A.elements()*sizeof(T));
    return out;
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 2);
    assert(args[1].dtype == DT_INT);
    if(args[0].dtype == DT_FLOAT) return { GPUKernel(*(args[0].t_float), *(args[1].t_int), workIndex) };
    else if(args[0].dtype == DT_INT) return { GPUKernel(*(args[0].t_int), *(args[1].t_int), workIndex) };
    else if(args[0].dtype == DT_LONG) return { GPUKernel(*(args[0].t_long), *(args[1].t_int), workIndex) };
    else {
      assert(false);
      return std::vector<TensorContainer>();
    }
  }

  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<int> &B, uint32_t * workIndex) {
    assert(B.dims() == 1);
    std::vector<size_t> shape(B.elements());
    int idx_arbitrary = -1;
    size_t elem = 1;
    const int * b_data = B.ptrShr(CPU); 
    for(size_t i = 0; i < B.elements(); i++) {
      int dim = b_data[i];
      if(dim < 0) {
        if(idx_arbitrary < 0) {
          idx_arbitrary = i;
        } else {
          fatal_error("Too many arbitrary dimensions in " + name());
        }
      } else {
        shape[i] = dim;
        elem *= dim;
      }
    }
    if(idx_arbitrary >= 0) {
      shape[idx_arbitrary] = A.elements() / elem;
    }
    TensorContainer out(DTypeMap<T>::type, shape);
    Tensor<T> *t_out = TensorExtractor<T>::Get(out);
    assert(t_out->elements() == A.elements());
    T * t_out_data = t_out->ptrInit(GPU);
    const T * a_data = A.ptrShr(GPU);
    cudaMemcpyAsync((void *)t_out_data, (void *)a_data, A.elements() * sizeof(T), cudaMemcpyDefault);
    cudaDeviceSynchronize();
    return out;
  }

#endif

};

