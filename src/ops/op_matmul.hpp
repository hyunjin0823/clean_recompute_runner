#pragma once
#include <algorithm>
#include <omp.h>
#include <mkl.h>

#include "common.h"

class MatMulOp : public BinaryOp {
  using BinaryOp::BinaryOp;
public:
  make_binary_compute(float)
  make_binary_compute(int)
  make_binary_compute(long)
  
  template<typename T>
  TensorContainer Kernel(Tensor<T> &A, Tensor<T> &B) {
    fatal_error("unimplemented");
  }
  TensorContainer Kernel(Tensor<float> &A, Tensor<float> &B) {
    //Get my info out of the input tensor
    int cornerX = 0;
    int cornerY = 0;
    int affectedWidth = 0;
    int affectedHeight = 0;
    int outputHeight = 0;
    int outputWidth = 0;
    int mode = 1;
    std::vector<std::vector<int> > boxes;
    std::vector<int> tempBox;

    if (A.bounding_boxes.size() > 0) {
      mode = 2;
      for (int q = 0; q < A.bounding_boxes.size(); q++) {
        tempBox.clear();
        cornerX = A.bounding_boxes[q][0];
        cornerY = A.bounding_boxes[q][1];
        affectedWidth = A.bounding_boxes[q][2];
        affectedHeight = A.bounding_boxes[q][3];
        outputWidth = A.bounding_boxes[q][4];
        outputHeight = A.bounding_boxes[q][5];
        //std::cout << "A Corner (" << cornerX << "," << cornerY << ")\n";
        //std::cout << "A Size is " << affectedWidth << " " << affectedHeight << std::endl;

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][0] < cornerX) {
          cornerX = B.bounding_boxes[q][0];
        }

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][1] < cornerY) {
          cornerY = B.bounding_boxes[q][1];
        }

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][2] > affectedWidth) {
          affectedWidth = B.bounding_boxes[q][2];
        }

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][3] > affectedHeight) {
          affectedHeight = B.bounding_boxes[q][3];
        }

        if (affectedHeight + cornerY > outputHeight)
        {
          affectedHeight = outputHeight - cornerY;
        }

        if (affectedWidth + cornerX > outputWidth)
        {
          affectedWidth = outputWidth - cornerX;
        }
        tempBox.push_back(cornerX);
        tempBox.push_back(cornerY);
        tempBox.push_back(affectedWidth);
        tempBox.push_back(affectedHeight);
        tempBox.push_back(outputWidth);
        tempBox.push_back(outputHeight);
        boxes.push_back(tempBox);
      }
    }

    //std::cout << "MODE " << mode << std::endl;
    
    //std::cout << "Corner (" << cornerX << "," << cornerY << ")\n";
    //std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl;

    if(A.dim(1) != B.dim(0))
      fatal_error("Dimension Mismatch in " + name());
    size_t m = A.dim(0), k = A.dim(1), n = B.dim(1);
    TensorContainer out(DT_FLOAT, {m, n});
    if(m*k*n < 128*128*128) {
      mkl_set_num_threads_local(parallelism);
      cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 
                  m, n, k, 1.0, A.ptr(), k, B.ptr(), n, 0.0, 
                  TensorExtractor<float>::Get(out)->ptrInit(), n);
      mkl_set_num_threads_local(0);
		} else {
      omp_parallel_gemm(A.ptr(), B.ptr(), TensorExtractor<float>::Get(out)->ptrInit(), 
										    m, k, n, parallelism);
    }
    
	if (mode == 2) {
      Tensor<float>& out_data = *(out.t_float);
      for (int q = 0; q < boxes.size(); q++) {
        out_data.bounding_boxes.push_back(boxes[q]);
      }
    }
    return out;
  }
private:
inline void omp_parallel_gemm(const float *A, const float *B, float *C,
                       size_t m, size_t k, size_t n, int threads) {
  #pragma omp parallel num_threads(threads)
  {
    mkl_set_num_threads_local(1);
    int tid = omp_get_thread_num();
    if(n > m) {
      size_t n_stride = (n + threads - 1) / threads;
      const float *B_start = B + tid * n_stride;
      float *C_start = C + tid * n_stride;
      size_t n_local = std::min(n - tid * n_stride, n_stride);
      cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n_local, k,
            1.0, A, k, B_start, n, 0.0, C_start, n);
    } else {
      size_t m_stride = (m + threads - 1) / threads;
      const float *A_start = A + tid * m_stride * k;
      float *C_start = C + tid * m_stride * n;
      size_t m_local = std::min(m - tid * m_stride, m_stride);
      cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m_local, n, k,
            1.0, A_start, k, B, n, 0.0, C_start, n);
    }
  }
}

#ifdef USE_CUDA
public:  
  make_binary_gpucompute(float);
  make_binary_gpucompute(int);
  make_binary_gpucompute(long);
  
  
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override {
    if(args.size() != 2) fatal_error("Incorrect arg count");
    switch(args[0].dtype) {
      case DT_FLOAT: return GPUCompute(*(args[0].t_float), *(args[1].t_float), workIndex);
      case DT_INT: return GPUCompute(*(args[0].t_int), *(args[1].t_int), workIndex);
      default:
        fatal_error(name());
        return std::vector<TensorContainer>();
    }
  }

  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex) {
    fatal_error("Unimplemented");
  }

  TensorContainer GPUKernel(Tensor<float> &A, Tensor<float> &B, uint32_t * workIndex);

#endif

};

