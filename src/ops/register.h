#include <string>

#include "common.h"

Op* CreateOp(std::string op_name, std::string instance_name, int parallelism);

void RegisterOps();

