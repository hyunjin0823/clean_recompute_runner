#ifdef USE_CUDA

#include "op_matmul.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define BLOCK_SIZE 16
#define NSMS 20
#define MAX_BLOCKS 8

__device__ uint get_smid() {
  uint ret;
  asm("mov.u32 %0, %smid;" : "=r"(ret));
  return ret;
}


__global__
void matmul_kernel(float * C, const float * A, const float * B, size_t m, size_t n, size_t k, 
                   size_t ntasks, size_t tasksize, size_t wtask,  uint32_t mask, uint32_t * workIndex) {
  
  if ( !(mask & (1<< get_smid()))) return;

  __shared__ size_t grabbedIndex;

  int tx = threadIdx.x;
  int ty = threadIdx.y;

  while(true) {
    if (tx == 0 && ty == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    int by = grabbedIndex / wtask;
    int bx = grabbedIndex % wtask;

    int aBegin = n * BLOCK_SIZE * by;
    int aEnd = aBegin + n - 1;
    int aStep = BLOCK_SIZE;

    int bBegin = BLOCK_SIZE * bx;
    int bStep = BLOCK_SIZE * k;
    
    float Csub = 0;

    for (int a = aBegin, b = bBegin; 
         a <= aEnd;
         a += aStep, b += bStep) {

      __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
      __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

      if ((a % n + tx) < n && (a / m) + ty < m) { 
        As[ty][tx] = A[a + n * ty + tx];
      } else {
        As[ty][tx] = 0;
      }
      
      if ((b % k + tx) < k && (b / n) + ty < n) {
        Bs[ty][tx] = B[b + k * ty + tx];
      } else {
        Bs[ty][tx] = 0;
      }

      __syncthreads();
      
      #pragma unroll
      for (int l = 0; l < BLOCK_SIZE; l++) {
        Csub += As[ty][l] * Bs[l][tx];
      }
    
      __syncthreads(); 
    }
  
    int c = k * BLOCK_SIZE * by + BLOCK_SIZE * bx;

    if( (c % k + tx) < k && (c / m + ty) < m) {
      C[c + k * ty + tx] = Csub;
    }
  }
}

TensorContainer MatMulOp::GPUKernel(Tensor<float> &A, Tensor<float> &B, uint32_t * workIndex) {

  if(A.dim(1) != B.dim(0)) {
    fatal_error("Dimension mismatch in " + name());
  }
  
  size_t m = A.dim(0);
  size_t n = A.dim(1);
  size_t k = B.dim(1);

  const float * A_data = A.ptrShr(GPU);
  const float * B_data = B.ptrShr(GPU);

  TensorContainer out(DT_FLOAT, {m,k} );
  float * out_data = TensorExtractor<float>::Get(out)->ptrInit(GPU);
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  size_t ntasks = ((m + BLOCK_SIZE - 1) / BLOCK_SIZE) * ((k + BLOCK_SIZE -1 ) / BLOCK_SIZE);
  size_t tasksize = (ntasks + parallelism - 1) / parallelism;
  size_t wtask = (k + BLOCK_SIZE - 1) / BLOCK_SIZE;

  dim3 mm_block(BLOCK_SIZE, BLOCK_SIZE);
  matmul_kernel<<<NSMS * MAX_BLOCKS, mm_block, 0>>>(out_data, A_data, B_data, m, n, k, ntasks, tasksize, wtask, mask, workIndex);
  cudaDeviceSynchronize();
  
  GPUScheduler::return_sms(mask);

  return out;
}

#endif
