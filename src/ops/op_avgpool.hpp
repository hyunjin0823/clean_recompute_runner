#pragma once

#include "common.h"

#include <omp.h>
#include <string>
#include <limits>
#include <algorithm>
#include <cassert>

class AvgPoolOp : public Op {
  using Op::Op;

 public:
  //For parallel job allocation
  inline std::pair<size_t, size_t> offset_stride(size_t dim, int tid, int threads) {
    size_t stride = dim / threads;
    size_t offset = stride * tid;
    size_t residual = dim - (stride * threads);
    if(tid < residual) return std::make_pair(offset + tid, stride + 1);
    else return std::make_pair(offset + residual, stride);
  }

  std::vector<TensorContainer> Compute( const std::vector<TensorContainer>& args) {
    assert(args.size() == 4);
    assert(args[0].dtype == DT_FLOAT);
    assert(args[1].dtype == DT_INT);
    assert(args[2].dtype == DT_INT);
    assert(args[3].dtype == DT_STRING);

    return { Kernel(*(args[0].t_float),
                    *(args[1].t_int),
                    *(args[2].t_int),
                    *(args[3].t_string))};
  }

  TensorContainer Kernel(Tensor<float>& input, Tensor<int>& kernel,
                                      Tensor<int>& strides, Tensor<std::string>& padding) {
    assert(input.dims() == 4);
    assert(kernel.dims() == 1);
    assert(kernel.dim(0) == 4);
    assert(strides.dims() == 1);
    assert(strides.dim(0) == 4);
    assert(kernel.get(0) == 1);
    assert(strides.get(0) == 1);

    int mode;
    int cornerX = 0;
    int cornerY = 0;
    int affectedWidth = 0;
    int affectedHeight = 0;
    size_t num_boxes;
    std::vector<int> first_patches;
    std::vector<int> patch_sizes;

    if(input.bounding_boxes.size() == 0)
    {
      mode = 1;
    }
    else
    {
      mode = 2;
      //Get my info out of the input tensor
    }

    //std::cout << "MODE " << mode << std::endl;

    const float * input_data = input.ptrShr();
    const int input_batches = input.dim(0);
    const int input_height = input.dim(1);
    const int input_width = input.dim(2);
    const int input_depth = input.dim(3);

    const int kernel_rows = kernel.get(1);
    const int kernel_cols = kernel.get(2);

    const int stride_rows = strides.get(1);
    const int stride_cols = strides.get(2);

    std::string padding_str = padding.val();

    //std::cout << "Averagepool " << input_height << " " << input_width << " " << kernel_cols << " " << kernel_rows << " " << stride_cols << " " << stride_rows << " ";
    
    // Determine output size based on padding method
    int output_height = 0;
    int output_width = 0;
    int output_depth = input_depth;
    if (!padding_str.compare("SAME")) {
      output_height = (input_height + stride_rows - 1) / stride_rows;
      output_width = (input_width + stride_cols - 1) / stride_cols;
    } else {
      output_height = (input_height - kernel_rows + stride_rows) / stride_rows;
      output_width = (input_width - kernel_cols + stride_cols) / stride_cols;
    }

    assert(output_height > 0);
    assert(output_width > 0);

    TensorContainer output_container(DT_FLOAT, {input_batches, output_height, output_width, output_depth});
    float* output_data;
    std::vector<std::vector<int> > recompute_data;

    if (mode == 1)
    {
      //if (mode == 2) {
      //  output_data = TensorExtractor<float>::Get(op_output)->ptrInit();
      //}
      //else {
      output_data = TensorExtractor<float>::Get(output_container)->ptrInit();
      //}
      std::fill_n(output_data, input_batches * output_height * output_width * output_depth, 0);

      // Define offset of the first tasks in the matrix
      int kernel_left_offset;
      int kernel_top_offset;
      if (!padding_str.compare("VALID")) {
        kernel_left_offset = 
            ((output_width - 1) * stride_cols + kernel_cols - input_width + 1) / 2;
        kernel_top_offset =
            ((output_height - 1) * stride_rows + kernel_rows - input_height + 1) / 2;
      } else {
        kernel_left_offset =
            ((output_width - 1) * stride_cols + kernel_cols - input_width) / 2;
        kernel_top_offset = 
            ((output_height - 1) * stride_rows + kernel_rows - input_height) / 2;
      }

      //std::cout << kernel_left_offset << " " << kernel_top_offset << std::endl;
      
      // Define number of tasks and task size
      const size_t num_tasks = input_batches * output_height * output_width;
      //const size_t task_block_size = (num_tasks + parallelism - 1) / parallelism;

      #pragma omp parallel for num_threads(parallelism)
      for (size_t parallel_index = 0; parallel_index < parallelism; parallel_index++) {
        std::pair<size_t, size_t> work = offset_stride(num_tasks, parallel_index, parallelism);
        const size_t task_block_start = work.first;
        const size_t task_block_end = work.first + work.second;

        // Perfrom a single avg pool
        for (size_t task_index = task_block_start; task_index < task_block_end; task_index++) {
          const size_t batch = task_index / (output_height * output_width);
          const size_t out_y = (task_index / output_width) % output_height;
          const size_t out_x = task_index % output_width;

          const float * input_batch_start = input_data + (batch * input_height * input_width * input_depth);
          const int in_y_origin = (out_y * stride_rows) - kernel_top_offset;
          const int in_x_origin = (out_x * stride_cols) - kernel_left_offset;
        
          const int left_zero_count = std::max(0, 0 - in_x_origin);
          const int right_zero_count = std::max(0, in_x_origin + kernel_cols - input_width);
          const int x_val = kernel_cols - (left_zero_count + right_zero_count);

          const int top_zero_count = std::max(0, 0 - in_y_origin);
          const int bottom_zero_count = std::max(0, in_y_origin + kernel_rows - input_height);
          const int y_val = kernel_rows - (top_zero_count + bottom_zero_count);
          
          float * output_data_start = output_data + task_index * output_depth;

          for (int kernel_y = 0; kernel_y < kernel_rows; kernel_y++) {
            const int in_y = in_y_origin + kernel_y;

            if ( in_y >= 0 && in_y < input_height ) {
              for (int kernel_x = 0; kernel_x < kernel_cols; kernel_x++) {
                const int in_x = in_x_origin + kernel_x;
                if (in_x >= 0 && in_x < input_width)  {
                  const float * in_data = input_batch_start + in_y * (input_width * input_depth) + in_x * input_depth;
                  for ( size_t d = 0; d < output_depth; d++) {
                    output_data_start[d] += in_data[d];
                  }
                }
              }
            }
          }

          for (size_t d = 0; d < output_depth; d++) {
            output_data_start[d] /= (x_val * y_val);
          }
        }
      }
    }

    if (mode == 2)
    {
      output_data = TensorExtractor<float>::Get(op_output)->ptrInit();
      //std::fill_n(output_data, input_batches * output_height * output_width * output_depth, 0);

      // Define offset of the first tasks in the matrix
      int kernel_left_offset;
      int kernel_top_offset;
      if (!padding_str.compare("VALID")) {
        kernel_left_offset = 
            ((output_width - 1) * stride_cols + kernel_cols - input_width + 1) / 2;
        kernel_top_offset =
            ((output_height - 1) * stride_rows + kernel_rows - input_height + 1) / 2;
      } else {
        kernel_left_offset =
            ((output_width - 1) * stride_cols + kernel_cols - input_width) / 2;
        kernel_top_offset = 
            ((output_height - 1) * stride_rows + kernel_rows - input_height) / 2;
      }

      //This is my function to find the info for the next layer, defined in ops.cpp and
      //common.h for the actual definition
      //
      //std::cout << "BEFORE" << input.bounding_boxes[0][2] << " " << input.bounding_boxes[0][3] << std::endl; 
      
      recompute_data = nextLayerRecompute(input_width, input_height, kernel_cols, kernel_rows,
                                          stride_cols, stride_rows, kernel_left_offset, kernel_top_offset,
                                          input.bounding_boxes, output_width, output_height);

      num_boxes = recompute_data.size();

      //Update the appropriate variables now that the function is done
      for (int q = 0; q < num_boxes; q++) {
        //Bounding boxes has cornerX in the 0th spot and cornerY in the 1st
        first_patches.push_back(((recompute_data[q][1] * output_width) + recompute_data[q][0]));
        //std::cout << "Average pool " << std::endl;
        //std::cout << "First Patches " << first_patches[q] << std::endl;
        //std::cout << recompute_data[q][0] << " " << recompute_data[q][1] << std::endl;

        //Bounding boxes has affected width in the 2nd spot and affected height in the 3rd
        patch_sizes.push_back(recompute_data[q][2] * recompute_data[q][3]);
        //std::cout << recompute_data[q][2] << " " << recompute_data[q][3] << std::endl;
        //std::cout << "Patch Sizes " << patch_sizes[q] << std::endl;
      }
      //std::cout << "SHOULD BE " << output_width << " " << output_height << std::endl;

      /*for (int q = 0; q < num_boxes; q++) {
        cornerX = recompute_data[q][0];
        cornerY = recompute_data[q][1];
        affectedWidth = recompute_data[q][2];
        affectedHeight = recompute_data[q][3];
        //std::cout << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << std::endl;
        for(int i = cornerY; i < cornerY + affectedHeight; i++) 
        {
          //std::fill_n(output_data+(i*output_width*output_depth)+(cornerX*output_depth), input_batches*affectedWidth*output_depth, 0);
          std::fill_n(output_data+(i*output_width*output_depth)+(cornerX*output_depth), affectedWidth*output_depth, 0);
        }
      }*/

      // Define number of tasks and task size
      //const size_t num_tasks = input_batches * output_height * output_width;
      //const size_t task_block_size = (num_tasks + parallelism - 1) / parallelism;

      for (int box_number = 0; box_number < num_boxes; box_number++) {
        cornerX = recompute_data[box_number][0];
        cornerY = recompute_data[box_number][1];
        affectedWidth = recompute_data[box_number][2];
        affectedHeight = recompute_data[box_number][3];
        #pragma omp parallel for num_threads(parallelism)
        for (size_t parallel_index = 0; parallel_index < parallelism; parallel_index++) {
          std::pair<size_t, size_t> work = offset_stride(patch_sizes[box_number], parallel_index, parallelism);
          const size_t task_block_start = work.first;
          const size_t task_block_end = work.first + work.second;

          // Perform a single avg pool
          for (size_t i = task_block_start; i < task_block_end; i++) {
            int row = i/affectedWidth;
            int col = (i%affectedWidth)+first_patches[box_number];
            int task_index = (col+(output_width*row));

            const size_t batch = task_index / (output_height * output_width);
            const size_t out_y = (task_index / output_width) % output_height;
            const size_t out_x = task_index % output_width;

            const float * input_batch_start = input_data + (batch * input_height * input_width * input_depth);
            const int in_y_origin = (out_y * stride_rows) - kernel_top_offset;
            const int in_x_origin = (out_x * stride_cols) - kernel_left_offset;
          
            const int left_zero_count = std::max(0, 0 - in_x_origin);
            const int right_zero_count = std::max(0, in_x_origin + kernel_cols - input_width);
            const int x_val = kernel_cols - (left_zero_count + right_zero_count);

            const int top_zero_count = std::max(0, 0 - in_y_origin);
            const int bottom_zero_count = std::max(0, in_y_origin + kernel_rows - input_height);
            const int y_val = kernel_rows - (top_zero_count + bottom_zero_count);
            
            float * output_data_start = output_data + task_index * output_depth;
            std::fill_n(output_data_start, output_depth, 0);

            for (int kernel_y = 0; kernel_y < kernel_rows; kernel_y++) {
              const int in_y = in_y_origin + kernel_y;

              if ( in_y >= 0 && in_y < input_height ) {
                for (int kernel_x = 0; kernel_x < kernel_cols; kernel_x++) {
                  const int in_x = in_x_origin + kernel_x;
                  if (in_x >= 0 && in_x < input_width)  {
                    const float * in_data = input_batch_start + in_y * (input_width * input_depth) + in_x * input_depth;
                    for ( size_t d = 0; d < output_depth; d++) {
                      output_data_start[d] += in_data[d];
                    }
                  }
                }
              }
            }

            for (size_t d = 0; d < output_depth; d++) {
              output_data_start[d] /= (x_val * y_val);
            }
          }
        }
      }
    }

    if(mode == 1)
    {
      op_output = output_container;
    }
    else if (mode == 2) {
      // Define offset of the first tasks in the matrix
      /*int kernel_left_offset;
      int kernel_top_offset;
      if (!padding_str.compare("VALID")) {
        kernel_left_offset = 
            ((output_width - 1) * stride_cols + kernel_cols - input_width + 1) / 2;
        kernel_top_offset =
            ((output_height - 1) * stride_rows + kernel_rows - input_height + 1) / 2;
      } else {
        kernel_left_offset =
            ((output_width - 1) * stride_cols + kernel_cols - input_width) / 2;
        kernel_top_offset = 
            ((output_height - 1) * stride_rows + kernel_rows - input_height) / 2;
      }

      //This is my function to find the info for the next layer, defined in ops.cpp and
      //common.h for the actual definition

      recompute_data = nextLayerRecompute(input_width, input_height, kernel_cols, kernel_rows,
                                          stride_cols, stride_rows, kernel_left_offset, kernel_top_offset,
                                          input.bounding_boxes, output_width, output_height);

      num_boxes = recompute_data.size();*/
      output_container = op_output;
      Tensor<float>& out_data = *(output_container.t_float);
      for (int z = 0; z < num_boxes; z++)
      {
        out_data.bounding_boxes.push_back(recompute_data[z]);
      }
    }

    return output_container;
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute( const std::vector<TensorContainer>& args, uint32_t * workIndex) {
    assert(args.size() == 4);
    assert(args[0].dtype == DT_FLOAT);
    assert(args[1].dtype == DT_INT);
    assert(args[2].dtype == DT_INT);
    assert(args[3].dtype == DT_STRING);
    assert(args[3].ttype == TT_SCALAR);

    return { Kernel(*(args[0].t_float),
                    *(args[1].t_int),
                    *(args[2].t_int),
                    *(args[3].t_string))};
  }

  TensorContainer GPUKernel(Tensor<float>& input, Tensor<int>& kernel,
                            Tensor<int>& strides, Tensor<std::string> &padding,
                            uint32_t * workIndex);

#endif

};
