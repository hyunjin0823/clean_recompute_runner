#include <iostream>
#include <mkl.h>
#include <omp.h>
#include <string>
#include <vector>
#include <cassert>

#include "common.h"

class ConvFunctor {
public:
  //For parallel job allocation, used many times below
  inline std::pair<size_t, size_t> offset_stride(size_t dim, int tid, int threads) {
    size_t stride = dim / threads;
    size_t offset = stride * tid;
    size_t residual = dim - (stride * threads);
    if(tid < residual) return std::make_pair(offset + tid, stride + 1);
    else return std::make_pair(offset + residual, stride);
  }

  void operator()(const float* input_data,
                  int input_batches, int input_height, int input_width,
                  int input_depth, const float* filter_data, int filter_height,
                  int filter_width, int filter_count, int stride_rows,
                  int stride_cols, std::string padding, float* output_data,
                  int output_height, int output_width, int parallelism, int mode,
                  std::vector<std::vector<int> > &bounding_boxes) {

    //Variable initializations used for my recompute code below
    //std::vector<std::vector<int> > recomputeData;
    int cornerX = 0;
    int cornerY = 0;
    int affectedWidth = 0;
    int affectedHeight = 0;
    int threshold = 144;
    int offset = 0;
    size_t total_patches = 0;
    size_t num_boxes;
    std::vector<int> first_patches;
    std::vector<int> patch_sizes;
    auto t1 = std::chrono::high_resolution_clock::now();
    auto t2 = std::chrono::high_resolution_clock::now();
    auto t3 = std::chrono::high_resolution_clock::now();
    //auto t4 = std::chrono::high_resolution_clock::now();
    //auto t5 = std::chrono::high_resolution_clock::now();
    auto t6 = std::chrono::high_resolution_clock::now();
    int delta = 0;
    //std::cout << "Layer size is " << input_width << " " << input_height << std::endl;
    //std::cout << "Corner (" << cornerX << "," << cornerY << ")\n";
    //std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl << std::endl;

    // These calculations define how the patches will be positioned within the
    // input image. The actual definitions are quite complex, and rely on the
    // previously-calculated output size.
    int filter_left_offset;
    int filter_top_offset;
    if (!padding.compare("VALID")) {
      filter_left_offset =
          ((output_width - 1) * stride_cols + filter_width - input_width + 1) / 2;
      filter_top_offset = ((output_height - 1) * stride_rows + filter_height - input_height + 1) / 2;
    } else {
      filter_left_offset = ((output_width - 1) * stride_cols + filter_width - input_width) / 2;
      filter_top_offset = ((output_height - 1) * stride_rows + filter_height - input_height) / 2;
    }

    //std::cout << filter_left_offset << " " << filter_top_offset << std::endl;


    //Only executed on a recompute run
    //This is my function to find the recompute info for the next layer, function is defined in ops.cpp and
    //common.h for the actual definition, it takes info about the current recompute area
    //and the charactertics of the current conv to find the recompute area for the next
    //layer, it will use this to know which patches to recompute
    if (mode == 2)
    {
      //Extra print statements for when we need extra info
      //std::cout << "Original corner is (" << cornerX << "," << cornerY << ")\n";
      //std::cout << "Width is " << affectedWidth << " by " << affectedHeight <<
      //  " and the layer size is " << input_width << " by " << input_height << std::endl;
      //std::cout << "Doing " << (((float)(affectedWidth*affectedHeight)/(input_width*input_height))*100) << " percent recomputation" << std::endl;
      //std::cout << "Doing " << (float)num_patches/patch_count << " percent recomputation" << std::endl;

      bounding_boxes = nextLayerRecompute(input_width, input_height, filter_width, filter_height,
                                          stride_cols, stride_rows, filter_left_offset, filter_top_offset,
                                          bounding_boxes, output_width, output_height);
      num_boxes = bounding_boxes.size();

      //Conv op information we may want to print
      //std::cout << "Conv2D " << input_height << " " << input_width << " " << filter_height << " " << filter_width << " " << stride_cols << " " << stride_rows << " ";
      //std::cout << "Conv2D Input Size: " << input_height << " " << input_width << " " << input_depth << " Num Kernels " << filter_count << " Kernel Size: " << filter_height << " " << filter_width << " Stride: " << stride_rows << " " << stride_cols << " Padding: " << filter_top_offset << " " << filter_left_offset << std::endl;

      //std::cout << "Next corner is (" << cornerX << "," << cornerY << ")\n";
      //std::cout << "Width is " << affectedWidth << " by " << affectedHeight << std::endl;
      //std::cout << "Next width is " << output_width << " by " << output_height << std::endl << std::endl;

      //Will be using first patch to know our top left corner, other shifts will be based
      //on this starting point
      for (int q = 0; q < num_boxes; q++) {
        //std::cout << "On Box " << q << std::endl;
        //Bounding boxes has cornerX in the 0th spot and cornerY in the 1st
        //std::cout << "Corners: " << bounding_boxes[q][0] << " " << bounding_boxes[q][1] << std::endl;
        first_patches.push_back(((bounding_boxes[q][1] * output_width) + bounding_boxes[q][0]));
        //std::cout << "First Patches " << first_patches[q] << std::endl;

        //Bounding boxes has affected width in the 2nd spot and affected height in the 3rd
        patch_sizes.push_back(bounding_boxes[q][2] * bounding_boxes[q][3]);
        //std::cout << "Patch Dims: " << bounding_boxes[q][2] << " " << bounding_boxes[q][3] << std::endl;
        total_patches += (bounding_boxes[q][2] * bounding_boxes[q][3]);
      }
      //std::cout << "Total Patches " << total_patches << std::endl;
      //std::cout << "We need to do MatMul with " << num_patches << " total rows" << std::endl;
    }

    //if(affectedWidth == input_width && affectedHeight == input_height)
    //{
    //  std::cout << "DOING FULL COMPUTATION NOW \n";
    //}

    //Dimension checking before we do the actual op
    if ((input_batches <= 0) || (input_width <= 0) || (input_height <= 0) ||
        (input_depth <= 0)) {
      fatal_error("Invalid input dimensions for convolution");
    }
    if ((filter_width <= 0) || (filter_height <= 0) || (filter_count <= 0)) {
      fatal_error("Invalid filter dimensions for convolution");
    }
    if ((output_width <= 0) || (output_height <= 0)) {
      fatal_error("Invalid output dimensions for 2d convolution");
    }

    // We can just use a GEMM if the im2col is the identity operator, e.g., if
    // the kernel is 1x1 or the input data and filter have same height/width.
    if (filter_height == 1 && filter_width == 1 && stride_rows == 1 && stride_cols == 1) {
      // The kernel is 1x1, this chunk does the original computation
      if (mode == 1)
      {
        const int m = input_batches * input_height * input_width;
        const int n = filter_count;
        const int k = input_depth;
        const int lda = k;
        const int ldb = filter_count;
        const int ldc = filter_count;
        const float alpha = 1.0;
        const float beta = 0.0;
        //std::cout << "0 " << "0" << std::endl;
        mkl_set_num_threads_local(parallelism);
        cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                    alpha, input_data, lda,
                    filter_data, ldb, 
                    beta,output_data, ldc);
        mkl_set_num_threads_local(0);
        return;
      }
      //Recompute version for 1x1 kernel
      else if (mode == 2)
      {
        float* im2col_buffer = (float*)std::malloc(total_patches * input_depth * sizeof(float));
        //std::cout << "Identity Conv, Theoretical Speed Up: " << 1.0/((float)num_patches/(input_height*input_width)) << std::endl;

        //Need to build the input matrix
        offset = 0;
        for (int box_number = 0; box_number < num_boxes; box_number++) {
          //std::cout << "Offset on box " << box_number << " is " << offset << std::endl;
          cornerX = bounding_boxes[box_number][0];
          cornerY = bounding_boxes[box_number][1];
          affectedWidth = bounding_boxes[box_number][2];
          affectedHeight = bounding_boxes[box_number][3];
          #pragma omp parallel for num_threads(parallelism) 
          for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
            //Start and ending patches for this parallel thread
            std::pair<size_t, size_t> work = offset_stride(patch_sizes[box_number], parallel_index, parallelism);
            const size_t patch_index_start = work.first;
            const size_t patch_index_end = work.first + work.second;

            for (int i = patch_index_start; i < patch_index_end; i++) {
              int row = i/affectedWidth;
              int col = (i%affectedWidth) + first_patches[box_number];
              size_t patch_index = (col+(output_width*row));

              //Take input_data in the patch_index spot and put it in im2col
              for(int q = 0; q < input_depth; q++)
              {
                im2col_buffer[((i+offset)*input_depth)+q] = input_data[(patch_index*input_depth)+q];
              }
            }
          }
          offset += patch_sizes[box_number];
        }

        //Need to compute the kernel by doing the MatMul
        float* temp = (float*)std::malloc(total_patches * filter_count * sizeof(float));
        const int m = input_batches * total_patches;
        const int n = filter_count;
        const int k = input_depth;
        const int lda = k;
        const int ldb = filter_count;
        const int ldc = filter_count;
        const float alpha = 1.0;
        const float beta = 0.0;
        mkl_set_num_threads_local(parallelism);
        cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                    alpha, im2col_buffer, lda,
                    filter_data, ldb, 
                    beta,temp, ldc);
        mkl_set_num_threads_local(0);

        //Need to update the intermediate results in the appropriate spots
        offset = 0;
        for (int box_number = 0; box_number < num_boxes; box_number++) {
          //std::cout << "Offset on box " << box_number << " is " << offset << std::endl << std::endl;
          cornerX = bounding_boxes[box_number][0];
          cornerY = bounding_boxes[box_number][1];
          affectedWidth = bounding_boxes[box_number][2];
          affectedHeight = bounding_boxes[box_number][3];
          #pragma omp parallel for num_threads(parallelism) 
          for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
            //Start and ending patches for this parallel thread
            std::pair<size_t, size_t> work = offset_stride(patch_sizes[box_number], parallel_index, parallelism);
            const size_t patch_index_start = work.first;
            const size_t patch_index_end = work.first + work.second;

            //Figure out what patch we have and where it is in the output
            for (int i = patch_index_start; i < patch_index_end; i++) {
              int row = i/affectedWidth;
              int col = (i%affectedWidth)+first_patches[box_number];
              size_t patch_index = (col+(output_width*row));
              for(int j = 0; j < filter_count; j++)
              {
                //Overwrite values that need to be updated
                output_data[(filter_count*patch_index)+j] = temp[(filter_count * (i+offset))+j];
              }
            }
          }
          offset += patch_sizes[box_number];
        }

        //t4 = std::chrono::high_resolution_clock::now();
        //delta = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count();
        //std::cout << "\n Recompute matmul " << delta << " microseconds \n \n";

        std::free(temp);
        std::free(im2col_buffer);

        return;
      }
    } else if (filter_height == input_height && filter_width == input_width &&
               !padding.compare("VALID")) {
      // The input data and filter have the same height/width.
      // Nothing fancy implemented here for recompute as we don't expect any speed up in
      // this case
      const int m = input_batches;
      const int n = filter_count;
      const int k = input_height * input_width * input_depth;
      const int lda = k;
      const int ldb = filter_count;
      const int ldc = filter_count;
      const float alpha = 1.0;
      const float beta = 0.0;
      //std::cout << "0 " << "0" << std::endl;
      mkl_set_num_threads_local(parallelism);
      cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                  alpha, input_data, lda,
                  filter_data, ldb, 
                  beta, output_data, ldc);
      mkl_set_num_threads_local(0);
      return;
    }

    //We now handle the most general case where the kernel is not 1x1 or the same size as
    //the input, this is the code that will be run most of the time
    //This first block is the conv op implementation when we do everything
    //High level is that we have to unroll the patches before doing the MatMul with the
    //kernel matrix
    if (mode == 1)
    {
      //t1 = std::chrono::high_resolution_clock::now();
      // The im2col buffer has # of patches rows, and # of filters cols.
      // It's laid out like this, in row major order in memory:
      //        < filter value count >
      //   ^   +---------------------+
      // patch |                     |
      // count |                     |
      //   v   +---------------------+
      // Each patch row contains a filter_width x filter_height patch of the
      // input, with the depth channel as the most contiguous in memory, followed
      // by the width, then the height. This is the standard memory order in the
      // image world if it helps to visualize it.
      const size_t filter_value_count = filter_width * filter_height * input_depth;
      const size_t patch_count = (input_batches * output_height * output_width);
      //std::cout << "We need to do MatMul with " << patch_count << " total rows" << std::endl;

      float* im2col_buffer = (float*)std::malloc(patch_count * filter_value_count * sizeof(float));

      //This is the parallel unpack of the input image to prepare to multiply it with the
      //kernel matrix
      #pragma omp parallel for num_threads(parallelism) 
      for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
        //Get the jobs this thread should be doing
        std::pair<size_t, size_t> work = offset_stride(patch_count, parallel_index, parallelism);
        const size_t patch_index_start = work.first;
        const size_t patch_index_end = work.first + work.second;
        //const size_t patch_index_end = std::min((size_t) (work.first + work.second), patch_count);

        for (size_t patch_index = patch_index_start; patch_index < patch_index_end; ++patch_index) {
          const size_t batch = patch_index / (output_height * output_width);
          const size_t out_y = (patch_index / output_width) % output_height;
          const size_t out_x = patch_index % output_width;
          const float* input_batch_start = input_data + (batch * input_height * input_width * input_depth);
          const int in_y_origin = (out_y * stride_rows) - filter_top_offset;
          const int in_x_origin = (out_x * stride_cols) - filter_left_offset;
          float* im2col_patch_start = im2col_buffer + (patch_index * filter_value_count);
          for (int filter_y = 0; filter_y < filter_height; ++filter_y) {
            const int in_y = in_y_origin + filter_y;
            float* im2col_row_start = im2col_patch_start + (filter_y * filter_width * input_depth);
            // If we're off the top or the bottom of the input, fill the
            // whole row with zeroes.
            if ((in_y < 0) || (in_y >= input_height)) {
              float* im2col_row_end = im2col_row_start + (filter_width * input_depth);
              std::fill(im2col_row_start, im2col_row_end, 0.0);
            } else {
              // What we're doing here is trying to copy and fill the im2col
              // buffer as efficiently as possible, using functions to set or
              // duplicate values en masse. We know we don't have to worry about
              // vertical edges because we dealt with that case above, so we
              // just need to handle filters that overlap the left or right
              // edges. Here's what that looks like:
              //
              // < left_zero_count > < center_copy_count > < right_zero_count >
              // +------------------+---------------------+--------------------+
              // |     (filter)     |       (image)       |      (filter)      |
              // +------------------+---------------------+--------------------+
              // in_x_origin        0                 input_width       in_x_end
              //
              // In reality it's unlikely that a filter patch will be wider
              // than an input, but this shows all the edge cases.
              // We use std::fill() to set the left and right sections to zeroes
              // and std::copy() to copy over the input data for the center.
              const int in_x_end = in_x_origin + filter_width;
              const int left_zero_count = std::max(0, 0 - in_x_origin);
              const int right_zero_count = std::max(0, in_x_end - input_width);
              const int center_copy_count =
                  filter_width - (left_zero_count + right_zero_count);
              if (left_zero_count > 0) {
                float* im2col_left_start = im2col_row_start;
                float* im2col_left_end =
                    im2col_left_start + (left_zero_count * input_depth);
                std::fill(im2col_left_start, im2col_left_end, 0.0);
              }
              if (center_copy_count > 0) {
                const float* input_row_start = 
                  input_batch_start + (in_y * input_width * input_depth) + 
                  (std::max(0, in_x_origin) * input_depth);
                const float* input_row_end = 
                  input_row_start + (center_copy_count * input_depth);
                float* im2col_center_start = 
                  im2col_row_start + (left_zero_count * input_depth);
                std::copy(input_row_start, input_row_end, im2col_center_start);
              }
              if (right_zero_count > 0) {
                float* im2col_right_start =
                    im2col_row_start +
                    ((left_zero_count + center_copy_count) * input_depth);
                float* im2col_right_end =
                    im2col_right_start + (right_zero_count * input_depth);
                std::fill(im2col_right_start, im2col_right_end, 0.0);
              }
            }
          }
        }
        // Now we've assembled a set of image patches into a matrix, apply a
        // GEMM matrix multiply of the patches as rows, times the filter
        // weights in columns, to get partial results in the output matrix.
      }

      //Timing information from unpack
      //t2 = std::chrono::high_resolution_clock::now();
      //delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      //std::cout << "\n Original unpack " << delta << " microseconds \n \n";

      //t1 = std::chrono::high_resolution_clock::now();

      //Threshold used to decide if parallelism is a good idea for the actual MatMul
      if(patch_count <= threshold)
      {
          const int m = input_batches * patch_count;
          const int n = filter_count;
          const int k = filter_value_count;
          const int lda = filter_value_count;
          const int ldb = filter_count;
          const int ldc = filter_count;
          const float alpha = 1.0;
          const float beta = 0.0;
          mkl_set_num_threads_local(parallelism);
          cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                      alpha, im2col_buffer, lda,
                      filter_data, ldb, 
                      beta,output_data, ldc);
          mkl_set_num_threads_local(0);
      }

      //If it is big enough, do the parallel version
      else
      {
        #pragma omp parallel for num_threads(parallelism)
        for (size_t parallel_index = 0; parallel_index < parallelism; parallel_index++) {
          std::pair<size_t, size_t> work = offset_stride(patch_count, parallel_index, parallelism);
          const size_t patch_index_start = work.first;
          const size_t patch_index_end = work.first + work.second;

          float* patch_start = im2col_buffer + patch_index_start * filter_value_count;
          float* out_start = output_data + patch_index_start * filter_count;

          const int how_many_patches = patch_index_end - patch_index_start;
          const int m = how_many_patches;
          const int n = filter_count;
          const int k = filter_value_count;
          const int lda = filter_value_count;
          const int ldb = filter_count;
          const int ldc = filter_count;
          const int alpha = 1.0;
          const int beta = 0.0;
          mkl_set_num_threads_local(1);
          cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                      alpha, patch_start, lda,
                      filter_data, ldb,
                      beta, out_start, ldc);
          mkl_set_num_threads_local(0);
        }
      }
      //t2 = std::chrono::high_resolution_clock::now();
      //delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      //std::cout << "\n Original matmul " << delta << " microseconds \n \n";
     
      //We are done now, the result of applying the kernel is in output_data and we can
      //return
      std::free(im2col_buffer);
    }
    // END original run

    //START This is the way we do things for the recomputation if mode is set to 2
    //High level is that we do the unroll for only the relevant patches, then do the
    //partial MatMul, and finally we have some extra work to handle the local updates
    else if (mode == 2)
    {
      //t3 = std::chrono::high_resolution_clock::now();
      const size_t filter_value_count = filter_width * filter_height * input_depth;
      //size_t patch_count = (input_batches * output_height * output_width);
      //std::cout << "Normal Conv, Theoretical Speed Up: " << 1.0/((float)num_patches/patch_count) << std::endl;
      
      float* im2col_buffer = (float*)std::malloc(total_patches * filter_value_count * sizeof(float));
      size_t patch_index;
      //int first_patch = ((cornerY * output_width) + cornerX);

      //This will do the unpack for only the relevant patches
      //Need to build the input matrix
      offset = 0;
      for (int box_number = 0; box_number < num_boxes; box_number++) {
        cornerX = bounding_boxes[box_number][0];
        cornerY = bounding_boxes[box_number][1];
        affectedWidth = bounding_boxes[box_number][2];
        affectedHeight = bounding_boxes[box_number][3];
        #pragma omp parallel for num_threads(parallelism) 
        for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
          //Start and ending patches for this parallel thread
          std::pair<size_t, size_t> work = offset_stride(patch_sizes[box_number], parallel_index, parallelism);
          const size_t patch_index_start = work.first;
          const size_t patch_index_end = work.first + work.second;

          for (int i = patch_index_start; i < patch_index_end; i++) {
            size_t row = i/affectedWidth;
            size_t col = (i%affectedWidth)+first_patches[box_number];
            //This computes one of the patches that will affect the output and unpacks it
            size_t patch_index = (col+(output_width*row));
            const size_t batch = patch_index / (output_height * output_width);
            const size_t out_y = (patch_index / output_width) % output_height;
            const size_t out_x = patch_index % output_width;
            const float* input_batch_start = input_data + (batch * input_height * input_width * input_depth);
            const int in_y_origin = (out_y * stride_rows) - filter_top_offset;
            const int in_x_origin = (out_x * stride_cols) - filter_left_offset;
            float* im2col_patch_start = im2col_buffer + ((i + offset) * filter_value_count);
            //float* im2col_patch_start = im2col_buffer + ((i) * filter_value_count);
            for (int filter_y = 0; filter_y < filter_height; ++filter_y) {
              const int in_y = in_y_origin + filter_y;
              float* im2col_row_start = im2col_patch_start + (filter_y * filter_width * input_depth);
              // If we're off the top or the bottom of the input, fill the
              // whole row with zeroes.
              if ((in_y < 0) || (in_y >= input_height)) {
                float* im2col_row_end = im2col_row_start + (filter_width * input_depth);
                std::fill(im2col_row_start, im2col_row_end, 0.0);
              } else {
                // What we're doing here is trying to copy and fill the im2col
                // buffer as efficiently as possible, using functions to set or
                // duplicate values en masse. We know we don't have to worry about
                // vertical edges because we dealt with that case above, so we
                // just need to handle filters that overlap the left or right
                // edges. Here's what that looks like:
                //
                // < left_zero_count > < center_copy_count > < right_zero_count >
                // +------------------+---------------------+--------------------+
                // |     (filter)     |       (image)       |      (filter)      |
                // +------------------+---------------------+--------------------+
                // in_x_origin        0                 input_width       in_x_end
                //
                // In reality it's unlikely that a filter patch will be wider
                // than an input, but this shows all the edge cases.
                // We use std::fill() to set the left and right sections to zeroes
                // and std::copy() to copy over the input data for the center.
                const int in_x_end = in_x_origin + filter_width;
                const int left_zero_count = std::max(0, 0 - in_x_origin);
                const int right_zero_count = std::max(0, in_x_end - input_width);
                const int center_copy_count =
                    filter_width - (left_zero_count + right_zero_count);
                if (left_zero_count > 0) {
                  float* im2col_left_start = im2col_row_start;
                  float* im2col_left_end =
                      im2col_left_start + (left_zero_count * input_depth);
                  std::fill(im2col_left_start, im2col_left_end, 0.0);
                }
                if (center_copy_count > 0) {
                  const float* input_row_start = 
                    input_batch_start + (in_y * input_width * input_depth) + 
                    (std::max(0, in_x_origin) * input_depth);
                  const float* input_row_end = 
                    input_row_start + (center_copy_count * input_depth);
                  float* im2col_center_start = 
                    im2col_row_start + (left_zero_count * input_depth);
                  std::copy(input_row_start, input_row_end, im2col_center_start);
                }
                if (right_zero_count > 0) {
                  float* im2col_right_start =
                      im2col_row_start +
                      ((left_zero_count + center_copy_count) * input_depth);
                  float* im2col_right_end =
                      im2col_right_start + (right_zero_count * input_depth);
                  std::fill(im2col_right_start, im2col_right_end, 0.0);
                }
              }
            }
          }
        }
        offset += patch_sizes[box_number];
        // Now we've assembled a set of image patches into a matrix, apply a
        // GEMM matrix multiply of the patches as rows, times the filter
        // weights in columns, to get partial results in the output matrix.
      }

      //t4 = std::chrono::high_resolution_clock::now();
      //delta = std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count();
      //std::cout << "\n Recompute unpack " << delta << " microseconds \n \n";

      //t5 = std::chrono::high_resolution_clock::now();
      
      float* temp = (float*)std::malloc(input_batches * total_patches * filter_count * sizeof(float));

      //Again decide based on the same threshold if we should parallelize the MatMul or not
      if(total_patches <= threshold)
      {
          const int m = input_batches * total_patches;
          const int n = filter_count;
          const int k = filter_value_count;
          const int lda = filter_value_count;
          const int ldb = filter_count;
          const int ldc = filter_count;
          const float alpha = 1.0;
          const float beta = 0.0;
          mkl_set_num_threads_local(parallelism);
          cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                      alpha, im2col_buffer, lda,
                      filter_data, ldb, 
                      beta,temp, ldc);
          mkl_set_num_threads_local(0);
      }

      //If it is big enough, parallelized MatMul will be faster
      else
      {
        #pragma omp parallel for num_threads(parallelism)
        for (size_t parallel_index = 0; parallel_index < parallelism; parallel_index++) {
          std::pair<size_t, size_t> work = offset_stride(total_patches, parallel_index, parallelism);
          const size_t patch_index_start = work.first;
          const size_t patch_index_end = work.first + work.second;

          float* patch_start = im2col_buffer + patch_index_start * filter_value_count;
          float* out_start = temp + patch_index_start * filter_count;

          const int how_many_patches = patch_index_end - patch_index_start;
          const int m = how_many_patches;
          const int n = filter_count;
          const int k = filter_value_count;
          const int lda = filter_value_count;
          const int ldb = filter_count;
          const int ldc = filter_count;
          const int alpha = 1.0;
          const int beta = 0.0;

          mkl_set_num_threads_local(1);
          cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                      alpha, patch_start, lda,
                      filter_data, ldb,
                      beta, out_start, ldc);
          mkl_set_num_threads_local(0);
        }
      }

      //Experimental idea to compare the current output to the output of the last layer
      //If parts didn't actually change, we may be able to shrink the recompute area

      /*float* old_area = (float*)std::malloc(num_patches * filter_count * sizeof(float));
      #pragma omp parallel for num_threads(parallelism) 
      for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
        //Start and ending patches for this parallel thread
        std::pair<size_t, size_t> work = offset_stride(num_patches, parallel_index, parallelism);
        const size_t patch_index_start = work.first;
        const size_t patch_index_end = work.first + work.second;

        for (int i = patch_index_start; i < patch_index_end; i++) {
          int row = i/affectedWidth;
          int col = (i%affectedWidth)+first_patch;
          size_t patch_index = (col+(output_width*row));
          for(int j = 0; j < filter_count; j++)
          {
            old_area[(filter_count*i)+j] = output_data[(filter_count*patch_index)+j];
          }
        }
      }

      //Have to do comparison before we update the intermediate results
      //Trying to check if no changes have been made and shrink size accordingly I think
      std::vector<int> newArea = minCompare(old_area, temp, input_depth, affectedWidth, affectedHeight); */

      //Do the local updates, we want to overwrite last runs output with only the relevant
      //results that our recompute run has computed
      //Is this offset being used correctly?
      offset = 0;
      for (int box_number = 0; box_number < num_boxes; box_number++) {
        cornerX = bounding_boxes[box_number][0];
        cornerY = bounding_boxes[box_number][1];
        affectedWidth = bounding_boxes[box_number][2];
        affectedHeight = bounding_boxes[box_number][3];
        #pragma omp parallel for num_threads(parallelism) 
        for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
          //Start and ending patches for this parallel thread
          std::pair<size_t, size_t> work = offset_stride(patch_sizes[box_number], parallel_index, parallelism);
          const size_t patch_index_start = work.first;
          const size_t patch_index_end = work.first + work.second;

          for (int i = patch_index_start; i < patch_index_end; i++) {
            int row = i/affectedWidth;
            int col = (i%affectedWidth)+first_patches[box_number];
            size_t patch_index = (col+(output_width*row));
            for(int j = 0; j < filter_count; j++)
            {
              //float diff = std::abs (output_data[(filter_count*patch_index)+j] - temp[(filter_count * i)+j]);
              //if (diff > 0.0)
              //{
              //  printf("%.9f %.9f %.9f \n", output_data[(filter_count*patch_index)+j], temp[(filter_count * i)+j], diff);
               // exit(0);
              //}
              output_data[(filter_count*patch_index)+j] = temp[(filter_count * (i+offset))+j];
            }
          }
        }
        offset += patch_sizes[box_number];
      }

      //Why are these not updating in the next layers?
      //Corner needs to change by that delta
      //cornerX += newArea[0];
      //cornerY += newArea[1];

      //Bounding box can change to some new size
      //affectedWidth = newArea[2];
      //affectedHeight = newArea[3];

      //t6 = std::chrono::high_resolution_clock::now();
      //delta = std::chrono::duration_cast<std::chrono::microseconds>(t6-t5).count();
      //delta = std::chrono::duration_cast<std::chrono::microseconds>(t6-t3).count();
      //std::cout << "\n Recompute matmul " << delta << " microseconds \n \n";
      
      std::free(temp);
    
      std::free(im2col_buffer);
    }
  }
};

class Conv2DOp : public Op {
  using Op::Op;

 public:
  std::vector<TensorContainer> Compute( const std::vector<TensorContainer>& args) override {
    assert(args.size() == 4);
    assert(args[0].dtype == DT_FLOAT);
    assert(args[1].dtype == DT_FLOAT);
    assert(args[2].dtype == DT_INT);
    assert(args[3].dtype == DT_STRING);
   
    return { Kernel(*(args[0].t_float),
                    *(args[1].t_float),
                    *(args[2].t_int),
                    *(args[3].t_string))};
  }

  TensorContainer Kernel
                        (Tensor<float>& input, Tensor<float>& filters,
                         Tensor<int>& strides, Tensor<std::string>& padding) {

    int mode;
    if(input.bounding_boxes.size() == 0)
    {
      mode = 1;
    }
    else
    {
      mode = 2;
    }

    assert(input.dims() == 4);
    assert(filters.dims() == 4);
    assert(strides.dims() == 1);
    assert(strides.dim(0) == 4);

    assert(input.dim(3) == filters.dim(2));

    const float* input_data = input.ptr();
    int input_batches = input.dim(0);
    int input_height = input.dim(1);
    int input_width = input.dim(2);
    int input_depth = input.dim(3);

    const float* filter_data = filters.ptr();
    int filter_height = filters.dim(0);
    int filter_width = filters.dim(1);
    int filter_count = filters.dim(3);

    int stride_rows = strides.get(1);
    int stride_cols = strides.get(2);

    std::string padding_str = padding.val();

    int output_height;
    int output_width;
    if (!padding_str.compare("SAME")) {
      output_height = (input_height + stride_rows - 1) / stride_rows;
      output_width  = (input_width + stride_cols - 1) / stride_cols;
    } else {
      output_height = (input_height - filter_height + stride_rows) / stride_rows;
      output_width  = (input_width - filter_width + stride_cols) / stride_cols;
    }

    //Deep copy so it doesn't affect other conv ops
    std::vector<std::vector<int> > copyBoxes;
    std::vector<int> copy;
    for (int w = 0; w < input.bounding_boxes.size(); w++) {
      copy.clear();
      //std::cout << input.bounding_boxes[w][0] << " " << input.bounding_boxes[w][1] << " " << input.bounding_boxes[w][2] << " " << input.bounding_boxes[w][3] << std::endl;
      copy.push_back(input.bounding_boxes[w][0]);
      copy.push_back(input.bounding_boxes[w][1]);
      copy.push_back(input.bounding_boxes[w][2]);
      copy.push_back(input.bounding_boxes[w][3]);
      copy.push_back(output_width);
      copy.push_back(output_height);
      copyBoxes.push_back(copy);
    }

    TensorContainer output_container(DT_FLOAT, {input_batches, output_height, output_width, filter_count});
    //float* output_data = TensorExtractor<float>::Get(output_container)->ptrInit();
    float* output_data;

    if (mode == 1)
    {
      //On original run, we need to put results into the output container
      output_data = TensorExtractor<float>::Get(output_container)->ptrInit();
    }

    //int size = input_batches * output_height * output_width * filter_count;
    if (mode == 2)
    {
      //Need to update op_output with changed results on recompute run
      output_data = TensorExtractor<float>::Get(op_output)->ptrInit();
      //std::cout << "SIZE " << copyBoxes.size() << std::endl;
    }

    //std::cout << "MODE " << mode << std::endl;
    
    assert(output_height > 0);
    assert(output_width > 0);

    //Functor takes all original params and my new ones
    ConvFunctor functor;
    functor(input_data, input_batches, input_height, input_width, input_depth,
            filter_data, filter_height, filter_width, filter_count,
            stride_rows, stride_cols, padding_str,
            output_data, output_height, output_width,
            parallelism, mode, copyBoxes);
    if(mode == 1)
    {
      //On original run, we need to store intermediate results, this op_output variable
      //will be persistant and will do that
      op_output = output_container;
    }
    else if (mode == 2)
    {
      //If we have made local changes to the old intermediate results, which the recompute
      //run did, then we need to update the tensorcontainer we return
      output_container = op_output;
      //Need to put recompute data in the output tensor for the next op to use
      Tensor<float>& out_data = *(output_container.t_float);
      //out_data.bounding_boxes = input.bounding_boxes;
      std::vector<int> bb;
      for (int z = 0; z < copyBoxes.size(); z++)
      {
        bb.clear();
        //std::cout << copyBoxes[z][0] << " " << copyBoxes[z][1] << " " << copyBoxes[z][2] << " " << copyBoxes[z][3] << std::endl;
        bb.push_back(copyBoxes[z][0]);
        bb.push_back(copyBoxes[z][1]);
        bb.push_back(copyBoxes[z][2]);
        bb.push_back(copyBoxes[z][3]);
        bb.push_back(output_width);
        bb.push_back(output_height);
        out_data.bounding_boxes.push_back(bb);
      }
    }

    //Then we can return the output of the layer with recompute info inside
    return output_container;

  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute( const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 4);
    assert(args[0].dtype == DT_FLOAT);
    assert(args[1].dtype == DT_FLOAT);
    assert(args[2].dtype == DT_INT);
    assert(args[3].dtype == DT_STRING);
    

    return { GPUKernel(*(args[0].t_float),
                       *(args[1].t_float),
                       *(args[2].t_int),
                       *(args[3].t_string),
                       workIndex)};
  }

  TensorContainer GPUKernel(Tensor<float>& input, Tensor<float>& filters,
                            Tensor<int>& strides, Tensor<std::string>& padding, uint32_t * workIndex);

#endif


};

