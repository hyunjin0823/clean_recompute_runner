#pragma once
#include <cmath>
#include <cstring>
#include <mkl.h>
#include <omp.h>

#include "common.h"

class ArgMaxOp : public BinaryOp {
  using BinaryOp::BinaryOp;
public:
  make_binary_compute(float);
  make_binary_compute(int);
  make_binary_compute(long);
  template<typename T>
  TensorContainer Kernel(Tensor<T> &A, Tensor<T> &B) {
    //Get my info out of the input tensor
    int cornerX = 0;
    int cornerY = 0;
    int affectedWidth = 0;
    int affectedHeight = 0;
    int outputWidth = 0;
    int outputHeight = 0;
    int mode = 1;
    std::vector<std::vector<int> > boxes;
    std::vector<int> tempBox;

    if (A.bounding_boxes.size() > 0) {
      mode = 2;
      for (int q = 0; q < A.bounding_boxes.size(); q++) {
        tempBox.clear();
        cornerX = A.bounding_boxes[q][0];
        cornerY = A.bounding_boxes[q][1];
        affectedWidth = A.bounding_boxes[q][2];
        affectedHeight = A.bounding_boxes[q][3];
        outputWidth = A.bounding_boxes[q][4];
        outputHeight = A.bounding_boxes[q][5];
        //std::cout << "A Corner (" << cornerX << "," << cornerY << ")\n";
        //std::cout << "A Size is " << affectedWidth << " " << affectedHeight << std::endl;

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][0] < cornerX) {
          cornerX = B.bounding_boxes[q][0];
        }

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][1] < cornerY) {
          cornerY = B.bounding_boxes[q][1];
        }

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][2] > affectedWidth) {
          affectedWidth = B.bounding_boxes[q][2];
        }

        if (B.bounding_boxes.size() > 0 && B.bounding_boxes[q][3] > affectedHeight) {
          affectedHeight = B.bounding_boxes[q][3];
        }
        tempBox.push_back(cornerX);
        tempBox.push_back(cornerY);
        tempBox.push_back(affectedWidth);
        tempBox.push_back(affectedHeight);
        tempBox.push_back(outputWidth);
        tempBox.push_back(outputHeight);
        boxes.push_back(tempBox);
      }
    }
    
    //std::cout << "Corner (" << cornerX << "," << cornerY << ")\n";
    //std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl;

    TensorContainer out(DTypeMap<T>::type, {A.dim(0)});
    const T* arr_A = A.ptrShr();
    T* arr_out = TensorExtractor<T>::Get(out)->ptrInit();
    #pragma omp parallel for num_threads(parallelism)
    for(size_t r = 0; r < A.dim(0); r++) {
      T rowmax = 0;
      const T* rowstart = arr_A + r * A.dim(1);
      for(size_t c = 0; c < A.dim(1); c++) {
        if(rowstart[c] > rowmax) {
          arr_out[r] = c;
          rowmax = rowstart[c];
        }
      }
    }

    if (mode == 2) {
      Tensor<float>& out_data = *(out.t_float);
      for (int q = 0; q < boxes.size(); q++) {
        out_data.bounding_boxes.push_back(boxes[q]);
      }
    }
    return out;
  }

#ifdef USE_CUDA
  make_binary_gpucompute(float);
  make_binary_gpucompute(int);
  make_binary_gpucompute(long);

  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override {
    if(args.size() != 2) fatal_error("Incorrect arg count");
    switch(args[0].dtype) {
      case DT_FLOAT: return GPUCompute(*(args[0].t_float), *(args[1].t_float), workIndex);
      case DT_INT: return GPUCompute(*(args[0].t_int), *(args[1].t_int), workIndex);
      default:
        fatal_error(name());
        return std::vector<TensorContainer>();
    }
  }

  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex);

#endif
};

