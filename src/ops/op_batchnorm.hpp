#pragma once
#include <cstring>
#include <cassert>

#include "common.h"

class BatchNormOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    //assert(args.size() == 5);
    //std::cout << "Batchnorm" << std::endl;
    for(int i = 0; i < 5; i++) {
      assert(args[i].ttype == TT_NORMAL);
      assert(args[i].dtype == DT_FLOAT);
    }
    size_t depth = args[0].shape()[args[0].shape().size() - 1];
    for(int i = 1; i < 5; i++) {
      assert(args[i].shape().size() == 1);
      assert(args[i].shape()[0] == depth);
    }
    //return { Kernel(*(args[0].t_float), *(args[1].t_float), *(args[2].t_float), *(args[3].t_float), *(args[4].t_float), *(args[5].t_int), *(args[6].t_int)) };
    return { args[0] };
  }

  std::vector<TensorContainer> Kernel(Tensor<float> &x, Tensor<float> &mean, Tensor<float> &variance, 
                         Tensor<float> &offset, Tensor<float> &scale, Tensor<int>& recompute, Tensor<int>& inMode) {
    const float *x_ptr = x.ptr();
    const float *mean_ptr = mean.ptr();
    const float *variance_ptr = variance.ptr();
    const float *offset_ptr = offset.ptr();
    const float *scale_ptr = scale.ptr();
    const size_t elements = x.elements();
    const size_t stride = x.dim(x.dims() - 1);
    const size_t num_strides = elements / stride;

    //Get my info out of the input tensor
    int cornerX = recompute.get(0);
    int cornerY = recompute.get(1);
    int affectedWidth = recompute.get(2);
    int affectedHeight = recompute.get(3);

    int mode = inMode.val();

    //mode = 1;

    //This has the output / intermediate results?
    TensorContainer out(DT_FLOAT, x.shape());
    //float *out_ptr = out.t_float->ptrInit();
    float *out_ptr;
    if (mode == 1)
    {
      cornerX = 0;
      cornerY = 0;
      affectedWidth = 0;
      affectedHeight = 0;
      out_ptr = out.t_float->ptrInit();
    }

    if (mode == 2)
    {
      out_ptr = TensorExtractor<float>::Get(op_output)->ptrInit();
    }

    //This is the actual computation, we do the full computation for both modes for now
    if(mode == 1 || mode == 2)
    {
      if(stride > num_strides) {
        for(size_t i = 0; i < num_strides; i++) {
          #pragma omp parallel for num_threads(parallelism)
          for(size_t j = 0; j < stride; j++) {
            out_ptr[i*stride+j] = scale_ptr[j] * (x_ptr[i*stride+j] - mean_ptr[j]) / sqrt(variance_ptr[j] + 0.001) + offset_ptr[j];
          }
        }
      } else 
      {
        #pragma omp parallel for num_threads(parallelism)
        for(size_t i = 0; i < num_strides; i++) {
          for(size_t j = 0; j < stride; j++) {
            out_ptr[i*stride+j] = scale_ptr[j] * (x_ptr[i*stride+j] - mean_ptr[j]) / sqrt(variance_ptr[j] + 0.001) + offset_ptr[j];
          }
        }
      }
    }
    else if(mode == 2)
    {
      if(stride > num_strides) {
        for(size_t i = 0; i < num_strides; i++) {
          #pragma omp parallel for num_threads(parallelism)
          for(size_t j = 0; j < stride; j++) {
            if(j >= cornerX && j < cornerX+affectedWidth && i >= cornerY && i < cornerY+affectedHeight)
            {
              out_ptr[i*stride+j] = scale_ptr[j] * (x_ptr[i*stride+j] - mean_ptr[j]) / sqrt(variance_ptr[j] + 0.001) + offset_ptr[j];
            }
          }
        }
      } else 
      {
        #pragma omp parallel for num_threads(parallelism)
        for(size_t i = 0; i < num_strides; i++) {
          for(size_t j = 0; j < stride; j++) {
            if(j >= cornerX && j < cornerX+affectedWidth && i >= cornerY && i < cornerY+affectedHeight)
            {
              out_ptr[i*stride+j] = scale_ptr[j] * (x_ptr[i*stride+j] - mean_ptr[j]) / sqrt(variance_ptr[j] + 0.001) + offset_ptr[j];
            }
          }
        }
      }
    }

    //Here I add my recomputation information into a TensorContainer and
    //put that info into the return vector
    TensorContainer recompute_output_container(DT_INT, {4});
    int* recomp_output_data = TensorExtractor<int>::Get(recompute_output_container)->ptrInit();
    recomp_output_data[0] = cornerX;
    recomp_output_data[1] = cornerY;
    recomp_output_data[2] = affectedWidth;
    recomp_output_data[3] = affectedHeight;

    if(mode == 1)
    {
      op_output = out;
    }
    else if (mode == 2)
    {
      out = op_output;
    }

    return {out, recompute_output_container};
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 5);
    for (int i = 0; i < 5; i++) {
      assert(args[i].ttype == TT_NORMAL);
      assert(args[i].dtype == DT_FLOAT);
    }
    size_t depth = args[0].shape()[args[0].shape().size() - 1];
    for (int i = 0; i < 5; i++) {
      assert(args[i].shape().size() == 1);
      assert(args[i].shape()[0] == depth);
    }
    return { GPUKernel(*(args[0].t_float),
                       *(args[1].t_float),
                       *(args[2].t_float),
                       *(args[3].t_float),
                       *(args[4].t_float),
                       workIndex) };
  }

  TensorContainer GPUKernel(Tensor<float> &x, Tensor<float> &mean, Tensor<float> &variance,
                            Tensor<float> &offset, Tensor<float> &scale, uint32_t * workIndex);
                       
  
#endif

};

