#pragma once
#include <cstring>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

class SqueezeOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    assert(args.size() == 2);
    assert(args[1].dtype == DT_INT);
    if(args[0].dtype == DT_FLOAT) return { Kernel(*(args[0].t_float), *(args[1].t_int)) };
    else if(args[0].dtype == DT_INT) return { Kernel(*(args[0].t_int), *(args[1].t_int)) };
    else {
      assert(false);
      return std::vector<TensorContainer>();
    }
  }
  template<typename T>
  TensorContainer Kernel(Tensor<T> &A, Tensor<int> &B) {
    std::vector<size_t> shape;
    for(size_t i = 0; i < A.dims(); i++) {
      if(A.dim(i) > 1) shape.push_back(A.dim(i));
      else {
        bool keep = true;
        for(size_t j = 0; j < B.elements(); j++) {
          if(B.ptr()[j] == i) keep = false;
        }
        if(keep) shape.push_back(A.dim(i));
      }
    }
    TensorContainer out(DTypeMap<T>::type, shape);
    T *p_out = TensorExtractor<T>::Get(out)->ptrInit();
    memcpy(p_out, A.ptr(), A.elements()*sizeof(T));
    return out;
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 2);
    assert(args[1].dtype == DT_INT);
    if(args[0].dtype == DT_FLOAT) return { GPUKernel(*(args[0].t_float), *(args[1].t_int), workIndex) };
    else if(args[0].dtype == DT_INT) return { GPUKernel(*(args[0].t_int), *(args[1].t_int), workIndex) };
    else {
      assert(false);
      return std::vector<TensorContainer>();
    }
  }
  
  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<int> &B, uint32_t * workIndex) {
    std::vector<size_t> shape;
    const int * b_data = B.ptrShr(CPU);
    for(size_t i = 0; i < A.dims(); i++) {
      if(A.dim(i) > 1) shape.push_back(A.dim(i));
      else {
        bool keep = true;
        for(size_t j = 0; j < B.elements(); j++) {
          if(b_data[j] == i) keep = false;
        }
        if(keep) shape.push_back(A.dim(i));
      }
    }
    TensorContainer out(DTypeMap<T>::type, shape);
    T *p_out = TensorExtractor<T>::Get(out)->ptrInit(GPU);
    const T * a_data = A.ptrShr(GPU);
    cudaMemcpyAsync((void *)p_out,
                    (void *)a_data,
                    A.elements()*sizeof(T),
                    cudaMemcpyDefault);
    cudaDeviceSynchronize();
    return out;
  }
  
#endif

};

