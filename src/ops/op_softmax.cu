#ifdef USE_CUDA

#include "op_softmax.hpp"

#include "cuda.h"
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"

#define BLOCK_SIZE 64
#define NSMS 20
#define MAX_BLOCKS 32
#define K 1

template<typename T>
__global__
void softmax_kernel(const T * in, T * out, size_t rows, size_t cols, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & (1 << smid))) return;

  int tid = threadIdx.x;

  __shared__ size_t grabbedIndex;
  __shared__ T max;
  __shared__ T sum;
  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if ( grabbedIndex >= rows) return;

    __shared__ T reduce[BLOCK_SIZE];

    if (tid < cols) reduce[tid] = in[grabbedIndex * cols + tid];

    for (int i = BLOCK_SIZE; i < cols; i += BLOCK_SIZE) {
      if (tid + i < cols) {
        T temp = in[ grabbedIndex * cols + i + tid];
        if ( temp > reduce[tid] ) reduce[tid] = temp;
      }
    }

    __syncthreads();

    #pragma unroll
    for ( int i = BLOCK_SIZE / 2; i > 0; i /= 2) {
      if ( tid < i) {
        if (reduce[tid + i] > reduce[tid]) {
          reduce[tid] = reduce[tid + i];
        }
      }
    }
    
    __syncthreads();

    if (tid == 0) max = reduce[tid];

    __syncthreads();

    reduce[tid] = 0;

    for (int i = 0; i < cols; i += BLOCK_SIZE) {
      if ( tid + i < cols) {
        T temp = exp((float)in[grabbedIndex * cols + i + tid] - max);
        reduce[tid] += temp;
        out[grabbedIndex * cols + i + tid] = temp;
      }
    }

    __syncthreads();

    #pragma unroll
    for ( int i = BLOCK_SIZE / 2; i > 0; i /= 2) {
      if ( tid < i) {
        reduce[tid] += reduce[tid + i];
      }
    }

    __syncthreads();

    if (tid == 0) sum = reduce[tid];

    __syncthreads();

    for ( int i = 0; i < cols; i += BLOCK_SIZE) {
      if ( tid + i < cols ) {
        out[grabbedIndex * cols + i + tid] /= sum;
      }
    }
  }
}

template<typename T>
TensorContainer SoftmaxOp::GPUKernel(Tensor<T> &A, uint32_t * workIndex) {
  assert(A.dims() == 2);
  TensorContainer out(DTypeMap<T>::type, A.shape());
  const T * arr_A = A.ptrShr(GPU);
  T * arr_out = TensorExtractor<T>::Get(out)->ptrInit(GPU);

  size_t rows = A.dim(0), cols = A.dim(1);
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);

  softmax_kernel<T><<<NSMS*MAX_BLOCKS, BLOCK_SIZE, 0>>>(arr_A, arr_out, rows, cols, mask, workIndex);

  cudaDeviceSynchronize();
  GPUScheduler::return_sms(mask);

  return out;

}

template TensorContainer SoftmaxOp::GPUKernel<float>(Tensor<float>& A, uint32_t * workIndex);
template TensorContainer SoftmaxOp::GPUKernel<long>(Tensor<long>& A, uint32_t * workIndex);
template TensorContainer SoftmaxOp::GPUKernel<int>(Tensor<int>& A, uint32_t * workIndex);

#endif

