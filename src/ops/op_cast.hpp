#pragma once

#include "common.h"

class CastOp : public Op {
  using Op::Op;
public:
  template<typename T1, typename T2>
  void cast_copy(Tensor<T1> &dst, Tensor<T2> &src) {
    size_t elements = src.elements();
    T1 *dst_ptr = dst.ptrInit();
    const T2 *src_ptr = src.ptr();
    #pragma omp parallel for num_threads(parallelism)
    for(size_t i = 0; i < elements; i++) {
      dst_ptr[i] = static_cast<T1>(src_ptr[i]);
    }
  }
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    assert(args.size() == 2);
    assert(args[0].ttype == TT_NORMAL);
    assert(args[1].dtype == DT_STRING);
    assert(args[1].ttype == TT_SCALAR);
    //TensorDType SrcT = args[0].dtype;
    //TensorDType DstT = string_dtype(args[1].t_string->val());
    std::cout << "In Cast \n";
    return  { args[0] };
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    return Compute(args);
  }
#endif

};

