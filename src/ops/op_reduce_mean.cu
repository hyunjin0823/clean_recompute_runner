#ifdef USE_CUDA

#include "op_reduce_mean.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define NSMS 20
#define MAX_BLOCKS 2
#define THREADS 1024

template<typename T>
__global__
void reduce_mean_kernel(T * out, const T * in, unsigned length, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= 1) return;

    int rstart = grabbedIndex;

    __shared__ T reduce[THREADS];

    reduce[tid] = 0;
    
    for (int i = 0; i < length; i += THREADS) {
      if (tid + i < length) {
        reduce [tid] += in[rstart * length + i + tid];
      }
    }

    __syncthreads();

    #pragma unroll 
    for (int i = THREADS / 2; i > 0; i /= 2) {
      if (tid < i) {
        reduce[tid] += reduce[tid + i];
      }
    }
    
    if (tid == 0) out[rstart] = reduce[0] / length;
  }
}

template<typename T>
TensorContainer ReduceMeanOp::GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex) {
  const T* arr_A = A.ptrShr(GPU);
  
  TensorContainer out(DTypeMap<T>::type, {1});
  T* out_val = TensorExtractor<T>::Get(out)->ptrInit(GPU);

  unsigned length = A.elements();

  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  reduce_mean_kernel<T><<<NSMS*MAX_BLOCKS, THREADS, 0>>>(out_val, arr_A, length, mask, workIndex);
  cudaDeviceSynchronize();

  GPUScheduler::return_sms(mask);

  return out;
}

template TensorContainer ReduceMeanOp::GPUKernel<float>(Tensor<float>&, Tensor<float>&, uint32_t *);
template TensorContainer ReduceMeanOp::GPUKernel<int>(Tensor<int>&, Tensor<int>&, uint32_t *);
template TensorContainer ReduceMeanOp::GPUKernel<long>(Tensor<long>&, Tensor<long>&, uint32_t *);

#endif

