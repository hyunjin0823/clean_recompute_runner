#pragma once
#include <cmath>
#include <cassert>
#include <vector>
#include <mutex>

#include "common.h"

std::mutex mtx;

/*    std::cout << "Corner (" << cornerX << "," << cornerY << ")\n"; \
    std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl; \
    std::cout << "Layer info is " << outputWidth << " " << outputHeight << std::endl; \*/

#define IMPLEMENT_OP(OP_NAME, ARG1, EXPRESSION) \
class OP_NAME : public UnaryOp { \
public: \
  OP_NAME(std::string n, int t) : UnaryOp(n, t) {} \
  \
  make_unary_compute(float); \
  make_unary_compute(int); \
  make_unary_compute(long); \
\
  template<typename T> \
  TensorContainer Kernel(Tensor<T> &A) { \
    int mode = 2; \
    int cornerX = 0; \
    int cornerY = 0; \
    int affectedWidth = 0; \
    int affectedHeight = 0; \
    int outputWidth = 0; \
    int outputHeight = 0; \
    int num_boxes = A.bounding_boxes.size(); \
    std::vector<std::vector<int> > tempBoxes; \
    std::vector<int> bb; \
    if (A.bounding_boxes.size() == 0) { \
      mode = 1; \
    } \
		TensorContainer out(DTypeMap<T>::type, A.shape()); \
		T *arr_out; \
		size_t elements = A.elements(); \
    if (mode == 1) { \
		  const T *arr_A = A.ptr(); \
		  arr_out = TensorExtractor<T>::Get(out)->ptrInit(); \
      run(arr_A, arr_out, elements); \
      op_output = out; \
    } \
    else if (mode == 2) { \
		  const T *arr_A = A.ptr(); \
		  arr_out = TensorExtractor<T>::Get(op_output)->ptrInit(); \
		  for (int q = 0; q < num_boxes; q++) { \
        cornerX = A.bounding_boxes[q][0]; \
        cornerY = A.bounding_boxes[q][1]; \
        affectedWidth = A.bounding_boxes[q][2]; \
        affectedHeight = A.bounding_boxes[q][3]; \
        outputWidth = A.bounding_boxes[q][4]; \
        outputHeight = A.bounding_boxes[q][5]; \
        recompRun(arr_A, arr_out, elements, cornerX, cornerY, affectedWidth, affectedHeight, outputWidth, outputHeight); \
        /* if (affectedWidth == 0 || affectedHeight == 0) { \ continue; \ } */ \
        bb.clear(); \
        /* std::cout << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << std::endl; */ \
        bb.push_back(cornerX); \
        bb.push_back(cornerY); \
        bb.push_back(affectedWidth); \
        bb.push_back(affectedHeight); \
        bb.push_back(outputWidth); \
        bb.push_back(outputHeight); \
        tempBoxes.push_back(bb); \
      } \
      out = op_output; \
    }\
    if (mode == 2) { \
      Tensor<float>& out_data = *(out.t_float); \
      for (int z = 0; z < tempBoxes.size(); z++) { \
        out_data.bounding_boxes.push_back(tempBoxes[z]); \
      }\
    } \
    \
		return out; \
	} \
\
  template<typename T> \
  void run(const T *A, T *out, size_t elements) { \
    /*int zero_count = 0;*/ \
    _Pragma("omp parallel for num_threads(parallelism) ") \
		for(size_t i = 0; i < elements; i++) { \
      T ARG1 = A[i]; \
			out[i] = EXPRESSION; \
		} \
	} \
\
  template<typename T> \
  void recompRun(const T *A, T *out, size_t elements, int &cornerX, int &cornerY, int &affectedWidth, int &affectedHeight, int outputWidth, int outputHeight) { \
    /* TODO Relies on input_batches being 1 */ \
    /* TODO Not doing dynamic recompute right now */ \
    int depth = elements / (outputWidth*outputHeight); \
    float thresh = 0 * depth; \
    int minX = outputWidth; \
    int maxX = 0; \
    int minY = outputHeight; \
    int maxY = 0; \
    _Pragma("omp parallel for num_threads(parallelism) ") \
    for(int i = cornerY; i < cornerY+affectedHeight; i++) { \
      float old_val = 0; \
      float new_val = 0; \
      float diff = 0.0; \
      for(int j = cornerX; j <  cornerX+affectedWidth; j++) { \
        diff = 0; \
        double temp = 0; \
        for(int k = 0; k < depth; k++) { \
          int spot = (i*outputWidth*depth)+(j*depth)+k; \
          old_val = out[spot]; \
          T ARG1 = A[spot]; \
          out[spot] = EXPRESSION; \
          new_val = out[spot]; \
          temp = std::max(new_val, old_val); \
          diff += (std::abs( new_val - old_val))/(std::max(temp, 1.0)); \
        } \
        if (diff > thresh) { \
          mtx.lock(); \
          if (j < minX) { minX = j; } \
          if (j > maxX) { maxX = j; } \
          if (i < minY) { minY = i; } \
          if (i > maxY) { maxY = i; } \
          mtx.unlock(); \
        } \
      }\
    }\
    int width = (maxX-minX) + 1; \
    int height = (maxY-minY) + 1; \
    if (width < 0) { width = 0;} \
    if (height < 0) { height = 0;} \
    if (height < affectedHeight) { affectedHeight = height; } \
    if (width < affectedWidth) { affectedWidth = width; } \
    cornerX = minX; \
    cornerY = minY; \
  }\
};

IMPLEMENT_OP(SigmoidOp, x, 1.0/(1+exp(-x)));
IMPLEMENT_OP(TanhOp, x, tanh(x));
IMPLEMENT_OP(RsqrtOp, x, 1.0/sqrt(x));
IMPLEMENT_OP(ReluOp, x, x > 0 ? x : 0);

#undef IMPLEMENT_OP

/*		for(size_t i = 0; i < elements; i++) { \
			if (out[i] < 0.00000000000000001) { \
			  zero_count++; \
      } \
		} \
    std::cout << "Ratio is " << (float)zero_count / elements << std::endl;
    int big_zero_count = 0; \
    std::cout << "Ratio in recompute area is " << ((float)zero_count / (affectedWidth*affectedHeight*depth))*100 << "% of zero elements" << std::endl; 
    std::vector<int> newArea = minCompare(old_area, new_area, depth, affectedWidth, affectedHeight);

        if (diff > 0) { \
          mtx.lock(); \
          if (j < minX) { minX = j; } \
          if (j > maxX) { maxX = j; } \
          if (i < minY) { minY = i; } \
          if (i > maxY) { maxY = i; } \
          mtx.unlock(); \
        } \
      } \
    } \
    int width = (maxX-minX) + 1; \
    int height = (maxY-minY) + 1; \
    if (width < 0) { width = 0;} \
    if (height < 0) { height = 0;} \
    if (height < affectedHeight) { affectedHeight = height; } \
    if (width < affectedWidth) { affectedWidth = width; } \
    cornerX = minX; \
    cornerY = minY; \

    printf("OLD corner (%d, %d) and is %d by %d \n", cornerX, cornerY, affectedWidth, affectedHeight ); \
    printf("NEW corner (%d, %d) and is %d by %d \n \n", cornerX, cornerY, affectedWidth, affectedHeight ); \
    */
