#pragma once

#include <memory>
#include <chrono>
#include <unordered_map>
#include <mutex>
#include <iostream>

#include "../def.h"

#define make_virtual_unary_compute(type) virtual std::vector<TensorContainer> Compute(Tensor<type>&)=0;
#define make_unary_compute(type) std::vector<TensorContainer> Compute(Tensor<type> &A) override { return {Kernel(A)}; }
#define make_virtual_binary_compute(type) virtual std::vector<TensorContainer> Compute(Tensor<type>&, Tensor<type>&)=0;
#define make_binary_compute(type) std::vector<TensorContainer> Compute(Tensor<type> &A, Tensor<type> &B) override { return { Kernel(A, B) }; }
//#define make_binary_compute(type) std::vector<TensorContainer> Compute(Tensor<type> &A, Tensor<type> &B) override { return Kernel(A, B) ; }

#ifdef USE_CUDA
#define make_virtual_unary_gpucompute(type) virtual std::vector<TensorContainer> GPUCompute(Tensor<type>&, uint32_t *)=0;
#define make_unary_gpucompute(type) std::vector<TensorContainer> GPUCompute(Tensor<type> &A, uint32_t * workIndex) override { return { GPUKernel(A, workIndex) }; }
#define make_virtual_binary_gpucompute(type) virtual std::vector<TensorContainer> GPUCompute(Tensor<type>&, Tensor<type>&, uint32_t *)=0;
#define make_binary_gpucompute(type) std::vector<TensorContainer> GPUCompute(Tensor<type> &A, Tensor<type> &B, uint32_t * workIndex) \
                                                                                        override { return { GPUKernel(A, B, workIndex) }; }
#define make_gpucompute_unimplemented() std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override { \
    std::cerr << "GPU Kernel for " << name() << " is not implemented. Using CPU Kernel.\n:"; \
    return Compute(args); }
#endif
std::vector<int> minCompare(const float*, const float*, int, int, int);

std::vector<std::vector<int> > nextLayerRecompute(int, int, int, int,
                                                  int, int, std::vector<std::vector<int> >,
                                                  int, int);

class Op {
protected:
  const std::string instance_name;
  TensorContainer op_output;
  float* unpacked_input;

public:
  int parallelism;

  Op(std::string n, int t);
  ~Op();

  std::vector<TensorContainer> TimedCompute(const std::vector<TensorContainer> &args, TargetDevice dev = CPU, 
                                            uint32_t * workIndex = nullptr);

  virtual std::vector<TensorContainer> Compute(const std::vector<TensorContainer>&)=0;

#ifdef USE_CUDA
  virtual std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>&,
                                                  uint32_t * workIndex)=0;
#endif

  std::string name() const;
};

class UnaryOp : public Op {
  using Op::Op;
public:
  make_virtual_unary_compute(float)
  make_virtual_unary_compute(int)
  make_virtual_unary_compute(long)

#ifdef USE_CUDA
  make_virtual_unary_gpucompute(float)
  make_virtual_unary_gpucompute(int)
  make_virtual_unary_gpucompute(long)
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override;
  
  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, uint32_t * workIndex);
#endif

  std::vector<TensorContainer> Compute(const std::vector<TensorContainer> &args);
  inline unsigned num_inputs() const;
};

class BinaryOp : public Op {
  using Op::Op;
public:
  make_virtual_binary_compute(float)
  make_virtual_binary_compute(int)
  make_virtual_binary_compute(long)

#ifdef USE_CUDA
  make_virtual_binary_gpucompute(float)
  make_virtual_binary_gpucompute(int)
  make_virtual_binary_gpucompute(long)
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override;

  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex);
#endif

  std::vector<TensorContainer> Compute(const std::vector<TensorContainer> &args);
  inline unsigned num_inputs() const;
};

class GPUScheduler {
private:
  
  static uint32_t occupied;

  static uint32_t n_sms;

  static std::mutex sm_lock;

public:
  
  static uint32_t schedule_sms(int parallelism);

  static void return_sms(uint32_t mask);

};

