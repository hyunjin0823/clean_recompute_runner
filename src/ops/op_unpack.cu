#ifdef USE_CUDA

#include "op_unpack.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#define NSMS 20
#define MAX_BLOCKS 8
#define THREADS 256

template<typename T>
__global__
void launch_unpack(T ** ptrs, int num, const T * in_data, size_t contig, size_t ntasks, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    for (size_t i = 0; i < contig * num; i += THREADS) {
      size_t offset = tid + i;
      ptrs[offset / contig][grabbedIndex * contig + offset % contig] = in_data[grabbedIndex * contig * num + offset];
    }
  }
}


template<typename T>
std::vector<TensorContainer> UnpackOp::GPUKernel(Tensor<T> &A, const int axis, uint32_t * workIndex) {
  assert(axis < A.dims());
  const size_t num = A.dim(axis);
  
  std::vector<TensorContainer> outs;
  outs.reserve(num);
  
  std::vector<T*> ptrs;
  ptrs.reserve(num);
  
  std::vector<size_t> shape(A.shape());
  shape.erase(shape.begin() + axis);
  for(int i = 0; i < num; i++) {
    outs.push_back(TensorContainer(DTypeMap<T>::type, shape));
    ptrs.push_back(TensorExtractor<T>::Get(outs[i])->ptrInit(GPU));
  }
  
  size_t stride = 1;
  for(size_t i = axis; i < A.dims(); i++)
    stride *= A.dim(i);
  size_t contig = stride / num;
  size_t segments = A.elements() / stride;
  const T * a_data = A.ptrShr(GPU);
  
  T ** d_ptrs;
  cudaMalloc((void **)d_ptrs, sizeof(T *) * num);
  cudaMemcpyAsync((void *)d_ptrs, (void *)&ptrs, sizeof(T *) * num, cudaMemcpyDefault);
  
  size_t ntasks = segments;
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);

  launch_unpack<T><<<NSMS*MAX_BLOCKS, THREADS, 0>>>(d_ptrs, num,
                                                    a_data, contig,
                                                    ntasks, mask, workIndex);
  
  GPUScheduler::return_sms(mask);
  cudaDeviceSynchronize();
  return outs;
}

template std::vector<TensorContainer> UnpackOp::GPUKernel<float>(Tensor<float> &A, const int axis, uint32_t * workIndex);

#endif


