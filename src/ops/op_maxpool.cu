#ifdef USE_CUDA

#include "op_maxpool.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <float.h>

#define NSMS 20
#define MAX_BLOCKS 64
#define THREADS 32

__global__
void maxpool_kernel(float * output_data, const float * input_data, 
                    const int input_batches, const int input_height, const int input_width, const int input_depth,
                    const int output_batches, const int output_height, const int output_width, const int output_depth,
                    const int kernel_cols, const int kernel_rows, const int stride_cols, const int stride_rows,
                    const int kernel_left_offset, const int kernel_top_offset,
                    const size_t ntasks, uint32_t mask, uint32_t * workIndex) { 

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    } 
    
    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    __shared__ float max[THREADS];
    max[tid] = -FLT_MAX;
    
    const size_t batch = grabbedIndex / (output_height * output_width);
    const size_t out_y = (grabbedIndex / output_width) % output_height;
    const size_t out_x = grabbedIndex % output_width;

    const float * input_batch_start = input_data + (batch * input_height * input_width * input_depth);
    const int in_y_origin = (out_y * stride_rows) - kernel_top_offset;
    const int in_x_origin = (out_x * stride_cols) - kernel_left_offset;
  
    float * output_data_start = output_data + grabbedIndex * output_depth;
    
    for (size_t k = 0; k < input_depth; k++) {
      for (size_t i = 0; i < kernel_rows; i++) {
        const int in_y = in_y_origin + i;
        if (in_y >= 0 && in_y < input_height) {
          for (size_t j = 0; j < kernel_cols; j++) {
            const int in_x = in_x_origin + j;
            if ( in_x >= 0 && in_x < input_width ) {
              max[tid] = ( input_batch_start[tid + k * THREADS] > max[tid]) ?  input_batch_start[tid + k * THREADS] : max[tid];
            }
          }
        }
      }
      output_data_start[tid + k * THREADS] = max[tid];
      max[tid] = -FLT_MAX;
    }
    __syncthreads();

  }
}

TensorContainer MaxPoolOp::GPUKernel(Tensor<float> &input, Tensor<int> &kernel,
                                     Tensor<int> &strides, Tensor<std::string> &padding, 
                                     uint32_t * workIndex) {
  assert(input.dims() == 4);
  assert(kernel.dims() == 1);
  assert(kernel.dim(0) == 4);
  assert(strides.dims() == 1);
  assert(strides.dim(0) == 4);
  assert(kernel.get(0) == 1);
  assert(strides.get(0) == 1);

  const float * input_data = input.ptrShr(GPU);
  const int input_batches = input.dim(0);
  const int input_height = input.dim(1);
  const int input_width = input.dim(2);
  const int input_depth = input.dim(3);

  const int kernel_rows = kernel.get(1);
  const int kernel_cols = kernel.get(2);

  const int stride_rows = strides.get(1);
  const int stride_cols = strides.get(2);

  std::string padding_str = padding.val();
  
  // Determine output size based on padding method
  int output_batches = input_batches;
  int output_height = 0;
  int output_width = 0;
  int output_depth = input_depth;
  if (!padding_str.compare("SAME")) {
    output_height = (input_height + stride_rows - 1) / stride_rows;
    output_width = (input_width + stride_cols - 1) / stride_cols;
  } else {
    output_height = (input_height - kernel_rows + stride_rows) / stride_rows;
    output_width = (input_width - kernel_cols + stride_cols) / stride_cols;
  }
  assert(output_height > 0);
  assert(output_width > 0);

  TensorContainer output_container(DT_FLOAT, {output_batches, output_height, output_width, output_depth});
  float * output_data = TensorExtractor<float>::Get(output_container)->ptrInit(GPU);
  
  // Define offset of the first tasks in the matrix
  int kernel_left_offset;
  int kernel_top_offset;
  if (!padding_str.compare("VALID")) {
    kernel_left_offset = 
        ((output_width - 1) * stride_cols + kernel_cols - input_width + 1) / 2;
    kernel_top_offset =
        ((output_height - 1) * stride_rows + kernel_rows - input_height + 1) / 2;
  } else {
    kernel_left_offset =
        ((output_width - 1) * stride_cols + kernel_cols - input_width) / 2;
    kernel_top_offset = 
        ((output_height - 1) * stride_rows + kernel_rows - input_height) / 2;
  }


  size_t ntasks = output_batches * output_height * output_width;

  uint32_t mask = GPUScheduler::schedule_sms(parallelism);

  maxpool_kernel<<<NSMS*MAX_BLOCKS, THREADS, 0>>>(output_data, input_data,
                                                  input_batches, input_height, input_width, input_depth,
                                                  output_batches, output_height, output_width, output_depth,
                                                  kernel_cols, kernel_rows, stride_cols, stride_rows,
                                                  kernel_left_offset, kernel_top_offset,
                                                  ntasks, mask, workIndex);
  cudaDeviceSynchronize();

  GPUScheduler::return_sms(mask);

  return output_container;
}

#endif



