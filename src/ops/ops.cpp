#include <sstream>
#include <memory>
#include <chrono>
#include <string>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cassert>
#include <mutex>
#include <math.h>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include "gpu_scheduler.cu"
#endif

// enable dimension printing
//#define VERBOSE

// enable fine-grained timing
//#define FINE_TIME

#ifdef FINE_TIME
std::unordered_map<std::string, std::vector<long>> op_times;
std::mutex op_times_lock;
#endif

Op::Op(std::string n, int t=1) : 
      parallelism(t), instance_name(n) {}

Op::~Op() {
#ifdef FINE_TIME
  auto it = op_times.find(name());
  if(it != op_times.end()) {
    size_t start = 0, size = it->second.size();
    if(size >= 1000) start = size - 1000;
    long sum = 0;
    for(size_t i = start; i < size; i++) {
      sum += it->second[i];
    }
    double avg = (double)sum / (double)(size-start);
    std::cout << "STAT " << name() << " " << avg << '\n';
  }
#endif
}

inline void print_dims(const TensorContainer &tc) {
  const auto shape = tc.shape();
  std::cout << " [";
  if(shape.size() > 0) std::cout << shape[0];
  for(int i = 1; i < shape.size(); i++)
    std::cout << ", " << shape[i];
  std::cout << "]";
}

//Find the minimal area that is different given 2 input areas
std::vector<int> minCompare(const float* oldT, const float* newT, int input_depth, int affectedWidth, int affectedHeight)
{
  //std::cout << "Input affected area is size " << affectedWidth << " " << affectedHeight << " with corner " << cornerX << " " << cornerY << std::endl;
  float diff = 0;
  int cornerX = 0;
  int cornerY = 0;

  //Initialize so they start at "max" values
  int minX = affectedWidth;
  int minY = affectedHeight;
  int maxX = 0;
  int maxY = 0;

  //cornerX and cornerY are initially 0
  for(int i = cornerY; i < cornerY + affectedHeight; i++)
  {
    for(int j = cornerX; j < cornerX + affectedWidth; j++)
    {
      diff = 0;
      for(int k = 0; k < input_depth; k++)
      {
        int spot = (i*affectedWidth*input_depth) + (j*input_depth) + k;
        diff += std::abs (newT[spot]-oldT[spot]);
      }
      //if (diff > thresh * input_depth)
      if (diff > 0.0)
      {
        if (j < minX)
        {
          minX = j;
        }
        if (j > maxX)
        {
          maxX = j;
        }
        if (i < minY)
        {
          minY = i;
        }
        if (i > maxY)
        {
          maxY = i;
        }
      }
    }
  }

  // Find bounding box size based on the min's and max's we found
  int width = (maxX-minX) + 1;
  int height = (maxY-minY) + 1;

  int total = 0;
  int same = 0;
  for(int i = minY; i < maxY; i++)
  {
    for(int j = minX; j < maxY; j++)
    {
      diff = 0;
      for(int k = 0; k < input_depth; k++)
      {
        int spot = (i*affectedWidth*input_depth) + (j*input_depth) + k;
        diff += std::abs (newT[spot]-oldT[spot]);
        //std::cout << "Old: " << oldT[spot] << " New: " << newT[spot] << std::endl;
      }
      //if (diff > thresh * input_depth)
      if (diff < 1e-7)
      {
        same++;
      }
      total++;
    }
  }
  //std::cout << "The ratio is " << (float(same)/total)*100 << std::endl;
  //std::cout << "Layer size is " << width << " by " << height << std::endl;

  //Extra check for weird sizes
  if (height > affectedHeight)
  {
    height = affectedHeight;
  }

  if (width > affectedWidth)
  {
    width = affectedWidth;
  }

  // If a dimension is 0, we don't need to do any recomputation
  if (width <= 0 || height <= 0){
    std::cout << "Zeroing" << std::endl;
    return {0,0,0,0};
  }

  if (width != affectedWidth || height != affectedHeight)
  {
    std::cout << "HERE" << std::endl;
    std::cout << "Input affected area is size " << affectedWidth << " " << affectedHeight << std::endl;
    std::cout << "Output affected area is size " << width << " " << height << std::endl;
  }

  return {minX, minY, width, height};
}

std::vector<std::vector<int> > nextLayerRecompute(int opInputWidth, int opInputHeight, int opKernelWidth, int opKernelHeight,
                                                  int opStrideCols, int opStrideRows, int opPaddingCols, int opPaddingRows,
                                                  std::vector<std::vector<int> > allBoxes, int outputWidth, int outputHeight) {

  std::vector<int> singleBox;
  std::vector<std::vector<int> > newBoxes;
  int newInputHeight = ((opInputHeight-opKernelHeight+(2*opPaddingRows))/opStrideRows)+1;
  int newInputWidth = ((opInputWidth-opKernelWidth+(2*opPaddingCols))/opStrideCols)+1;
  int topPad = 0;
  int bottomPad = 0;
  int leftPad = 0;
  int rightPad = 0;
  int newAffectedHeight = 0;
  int newAffectedWidth = 0;
  int cornerX = 0;
  int cornerY = 0;
  int affectedWidth = 0;
  int affectedHeight = 0;
  int counter = 0;
  int firstCornerX = 0;
  int firstCornerY = 0;
  int firstAffectedWidth = 0;
  int firstAffectedHeight = 0;
  int xOverlap = 0;
  int yOverlap = 0;
  bool merged = false;

  //std::cout << "There are " << allBoxes.size() << " boxes coming in" << std::endl;

  for (int q = 0; q < allBoxes.size(); q++) {
    cornerX = allBoxes[q][0];
    cornerY = allBoxes[q][1];
    affectedWidth = allBoxes[q][2];
    affectedHeight = allBoxes[q][3];
    topPad = 0;
    bottomPad = 0;
    leftPad = 0;
    rightPad = 0;
    newAffectedHeight = 0;
    newAffectedWidth = 0;

    //If a box has either dimension 0, there is no reason to track it
    if (affectedWidth <= 0 || affectedHeight <= 0)
    {
      singleBox.clear();
      /*singleBox.push_back(0);
      singleBox.push_back(0);
      singleBox.push_back(0);
      singleBox.push_back(0);
      singleBox.push_back(outputWidth);
      singleBox.push_back(outputHeight);
      newBoxes.push_back(singleBox);*/
      continue;
    }

    //If any box fills the whole layer, get rid of all other boxes and do the full
    //recomputation
    if (affectedWidth == opInputWidth && affectedHeight == opInputHeight)
    {
      singleBox.clear();
      newBoxes.clear();
      singleBox.push_back(cornerX);
      singleBox.push_back(cornerY);
      singleBox.push_back(affectedWidth);
      singleBox.push_back(affectedHeight);
      //singleBox.push_back(outputWidth);
      //singleBox.push_back(outputHeight);
      newBoxes.push_back(singleBox);
      return newBoxes;
    }

    //For HEIGHT
    //Check if we have run into the top of the input, but not the bottom
    if (((cornerY-opKernelHeight+opPaddingRows+1) < 0) && ((cornerY+affectedHeight+opKernelHeight-opPaddingRows-1) <= opInputHeight))
    {
      //Full (kernel - 1) size padding on bottom and as much padding as possible on the top
      topPad = cornerY+opPaddingRows;
      bottomPad = opKernelHeight-1;
      newAffectedHeight = (floor((float)(affectedHeight-opKernelHeight+topPad+bottomPad)/opStrideRows)+1);
                    
      //If we have hit the top, upper coordinate must be 0
      cornerY = 0;
    }

    //Check if we have run into the bottom of the input but not the top
    else if (((cornerY+affectedHeight+opKernelHeight-opPaddingRows-1) > opInputHeight) && ((cornerY-opKernelHeight+opPaddingRows+1) >= 0))
    {
      //Full (kernel - 1) size padding on top and as much padding as possible on bottom
      topPad = opKernelHeight-1;
      bottomPad = (opInputHeight + opPaddingRows) - (cornerY + affectedHeight);
      newAffectedHeight = (floor((float)(affectedHeight-opKernelHeight+topPad+bottomPad)/opStrideRows)+1);

      //Find the upper corner coordinate by seeing how many invocations of
      //the kernel you need to reach the affected area
      cornerY = (floor((float)(cornerY+opPaddingRows-opKernelHeight)/opStrideRows))+1;
      if (cornerY < 0)
      {
        cornerY = 0;
      }
      if (cornerY > opInputHeight)
      {
        cornerY = opInputHeight;
      }
    }

    //We have not run into the top or bottom
    else if (((cornerY-opKernelHeight+opPaddingRows+1) >= 0) && ((cornerY+affectedHeight+opKernelHeight-opPaddingRows-1) <= opInputHeight))
    {
      //Full (kernel - 1) padding on top and bottom
      topPad = opKernelHeight-1;
      bottomPad = topPad;
      //Size has changed, we need to move the corner
      newAffectedHeight = (floor((float)(affectedHeight-opKernelHeight+topPad+bottomPad)/opStrideRows)+1);

      //Find the upper corner coordinate
      cornerY = (floor((float)(cornerY+opPaddingRows-opKernelHeight)/opStrideRows))+1;
      if (cornerY < 0)
      {
        cornerY = 0;
      }
      if (cornerY > opInputHeight)
      {
        cornerY = opInputHeight;
      }
    }

    //This means we are bounded on both dims for the height
    else
    {
      //Make affected area the same as the input
      newAffectedHeight = newInputHeight;
      cornerY = 0;
    }

    //Update this no matter what
    affectedHeight = newAffectedHeight;

    //For WIDTH
    //Check if we have run into the left bound of the input, but not the right
    if (((cornerX-opKernelWidth+opPaddingCols+1) < 0) && ((cornerX+affectedWidth+opKernelWidth-opPaddingCols-1) <= opInputWidth))
    {
      //Full (kernel - 1) size padding on right and as much padding as possible on left
      leftPad = cornerX+opPaddingRows;
      rightPad = opKernelWidth-1;
      newAffectedWidth = (floor((float)(affectedWidth-opKernelWidth+leftPad+rightPad)/opStrideCols)+1);
                    
      //If we have hit the left, x coordinate must be 0
      cornerX = 0;
    }

    //Check if we have run into the right bound of the input, but not the left
    else if (((cornerX+affectedWidth+opKernelWidth-opPaddingCols-1) > opInputWidth) && ((cornerX-opKernelWidth+opPaddingCols+1) >= 0))
    {
      //Full (kernel - 1) size padding on left and as much padding as possible on right
      leftPad = opKernelWidth-1;
      rightPad = (opInputWidth+opPaddingCols) - (cornerX+affectedWidth);
      newAffectedWidth = (floor((float)(affectedWidth-opKernelWidth+leftPad+rightPad)/opStrideCols)+1);

      //Find the upper corner x coordinate by seeing how many invocations of
      //the kernel you need to reach the affected area
      cornerX = (floor((float)(cornerX+opPaddingCols-opKernelWidth)/opStrideCols))+1;
      if (cornerX < 0)
      {
        cornerX = 0;
      }
      if (cornerX > opInputWidth)
      {
        cornerX = opInputWidth;
      }
    }

    //We have not run into the left or right bound
    else if (((cornerX-opKernelWidth+opPaddingCols+1) >= 0) && ((cornerX+affectedWidth+opKernelWidth-opPaddingCols-1) <= opInputWidth))
    {
      //Full (kernel - 1) padding on left and right
      leftPad = opKernelWidth-1;
      rightPad = leftPad;
      //Size has changed, we need to move the corner
      newAffectedWidth = (floor((float)(affectedWidth-opKernelWidth+leftPad+rightPad)/opStrideCols)+1);

      //Find the upper corner x coordinate by seeing how many invocations of
      //the kernel you need to reach the affected area
      cornerX = (floor((float)(cornerX+opPaddingCols-opKernelWidth)/opStrideCols))+1;
      if (cornerX < 0)
      {
        cornerX = 0;
      }
      if (cornerX > opInputWidth)
      {
        cornerX = opInputWidth;
      }
    }

    //This means we are bounded on both dims for the width
    else
    {
      //Make affected area the same as the input
      newAffectedWidth = newInputWidth;
      cornerX = 0;
      //smallAreaDim2 = affectedWidth + (2*opPaddingCols)
    }

    //Update this no matter what
    affectedWidth = newAffectedWidth;

    //Extra check for weird sizes
    if (affectedHeight + cornerY > outputHeight)
    {
      affectedHeight = outputHeight - cornerY;
    }

    if (affectedWidth + cornerX > outputWidth)
    {
      affectedWidth = outputWidth - cornerX;
    }

    merged = false;
    for (int q = 0; q < newBoxes.size(); q++)
    {
      firstCornerX = newBoxes[q][0];
      firstCornerY = newBoxes[q][1];
      firstAffectedWidth = newBoxes[q][2];
      firstAffectedHeight = newBoxes[q][3];
      xOverlap = (std::min((firstCornerX + firstAffectedWidth), (cornerX + affectedWidth)) - std::max(cornerX, firstCornerX));
      yOverlap = (std::min((firstCornerY + firstAffectedHeight), (cornerY + affectedHeight)) - std::max(cornerY, firstCornerY));
      if (xOverlap > 0 && yOverlap > 0 && (((xOverlap * yOverlap) / (affectedWidth * affectedHeight)) > 0.90))
      {
        //Then want to merge
        //std::cout << "MERGING " << newBoxes[q][0] << " " << newBoxes[q][1] << " " << newBoxes[q][2] << " " << newBoxes[q][3] << 
        //  " with " << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << std::endl;
        newBoxes[q][0] = std::min(firstCornerX, cornerX);
        newBoxes[q][1] = std::min(firstCornerY, cornerY);
        newBoxes[q][2] = firstAffectedWidth + affectedWidth - xOverlap;
        newBoxes[q][3] = firstAffectedHeight + affectedHeight - yOverlap;
        merged = true;
        //std::cout << "AFTER " << newBoxes[q][0] << " " << newBoxes[q][1] << " " << newBoxes[q][2] << " " << newBoxes[q][3] << std::endl;
        //exit(0);
        break;
      }
    }

    if (!merged)
    {
      singleBox.clear();
      singleBox.push_back(cornerX);
      singleBox.push_back(cornerY);
      singleBox.push_back(affectedWidth);
      singleBox.push_back(affectedHeight);
      //singleBox.push_back(outputWidth);
      //singleBox.push_back(outputHeight);
      newBoxes.push_back(singleBox);
    }
    counter++;
  }

  //std::cout << "There are " << newBoxes.size() << " boxes left" << std::endl;
  //std::cout << "REMOVED: " << counter - newBoxes.size() << std::endl;
  //exit(0);
  //std::cout << outputWidth << " " << outputHeight << std::endl;
  /*if (newBoxes.size() <= 5)
  {
    for (int i = 0; i < newBoxes.size(); i++)
    {
      std::cout << newBoxes[i][0] << " " << newBoxes[i][1] << " " << newBoxes[i][2] << " " << newBoxes[i][3] << std::endl;
    }
  } */
  return newBoxes;

  /*std::cout << "On the next layer, size is " << newInputWidth << " by " << newInputHeight << 
    " and the corner is (" << cornerX << "," << cornerY << ")" <<
    " with size " << affectedWidth << " by " << affectedHeight << std::endl;*/
}

template<typename T>
void write_tensor(const std::string op_name, Tensor<T> &tensor) {
  std::string fname = op_name;
  std::replace(fname.begin(), fname.end(), '/', '-');
  std::string path = "temp/gpu/" + fname;
  std::ofstream outfile(path.c_str());
  outfile << dtype_string(DTypeMap<T>::type) << '\n';
  outfile << tensor.dims();
  for(size_t i = 0; i < tensor.dims(); i++) {
    outfile << ' ' << tensor.dim(i);
  }
  outfile << '\n';
  const T *ptr = tensor.ptrShr();
  for(size_t i = 0; i < tensor.elements(); i++) {
    outfile << ptr[i] << '\n';
  }
  outfile.close();
}

std::vector<TensorContainer> Op::TimedCompute(const std::vector<TensorContainer> &args, TargetDevice dev, uint32_t * workIndex) {
  std::vector<TensorContainer> res; 
#ifdef VERBOSE
  std::cout << name() << " {";
  for(auto tc : args) {
    print_dims(tc);
  }
  std::cout << " }\n";
#endif
#ifdef FINE_TIME
  auto t1 = std::chrono::high_resolution_clock::now();
#endif
  if (dev == CPU) {
    res = Compute(args);
  } else {
#ifdef USE_CUDA
    res = GPUCompute(args, workIndex);
#endif
  }
#ifdef FINE_TIME
  auto t2 = std::chrono::high_resolution_clock::now();
  op_times_lock.lock();
  op_times[name()].push_back(std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count());
  op_times_lock.unlock();
#endif
#ifdef VERBOSE
  std::cout << "-> {";
  for(const auto &tc : res) {
    print_dims(tc);
  }
  std::cout << " }\n\n";

  /*for(size_t i = 0; i < res.size(); i++) {
    std::cout << "Results for op " << name() << "\n";
    switch(res[i].dtype) {
    case DT_FLOAT:
      write_tensor(name() + ":" + std::to_string(i), *(res[i].t_float));
      break;
    case DT_INT:
      write_tensor(name() + ":" + std::to_string(i), *(res[i].t_int));
      break;
    default:
      std::cout << "No output for: " << name() << ':' << i << '\n';
      break;
    }
  }*/
#endif

  return res;
}

#ifdef USE_CUDA
/*
std::vector<TensorContainer> Op::GPUCompute(const std::vector<TensorContainer>& args) {
  std::cerr << "GPU Kernel for " + name() + " is not implemented. Using CPU kernel.\n";
  return Compute(args); 
}*/

std::vector<TensorContainer> UnaryOp::GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) {
  std::cerr << "GPU Kernel for " + name() + " is not implemented. Using CPU kernel.\n";
  return Compute(args); 
}

std::vector<TensorContainer> BinaryOp::GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) {
  std::cerr << "GPU Kernel for " + name() + " is not implemented. Using CPU kernel.\n";
  return Compute(args); 
}

template <typename T>
TensorContainer UnaryOp::GPUKernel(Tensor<T> &A, uint32_t * workIndex) {
  fatal_error("SHOULD NEVER BE CALLED");
}

template <typename T>
TensorContainer BinaryOp::GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex) {
  fatal_error("SHOULD NEVER BE CALLED");
}
#endif

std::string Op::name() const { return instance_name; }

std::vector<TensorContainer> UnaryOp::Compute(const std::vector<TensorContainer> &args) {
  if(args.size() != 1) fatal_error("Incorrect arg count");
  switch(args[0].dtype) {
    case DT_FLOAT: return Compute(*(args[0].t_float));
    case DT_INT: return Compute(*(args[0].t_int));
    default:
      fatal_error(name());
      return std::vector<TensorContainer>();
  }
}

std::vector<TensorContainer> BinaryOp::Compute(const std::vector<TensorContainer> &args) {
  if(args.size() != 2) fatal_error("Incorrect arg count");
  switch(args[0].dtype) {
  case DT_FLOAT: return Compute(*(args[0].t_float), *(args[1].t_float));
  //case DT_DOUBLE: return Compute(*(args[0].t_double), *(args[1].t_double));
  case DT_INT: return Compute(*(args[0].t_int), *(args[1].t_int));
  case DT_LONG: return Compute(*(args[0].t_long), *(args[1].t_long));
  default:
    fatal_error(name() + " got DT_NONE");
    return std::vector<TensorContainer>();
  }
}

#include "register.h"

template<typename T> 
Op* opFactory(std::string name, int parallelism) { 
  return new T(name, parallelism);
}
std::unordered_map<std::string, Op*(*)(std::string, int)> op_name_to_factory;
#define RegisterOp(op_name, class_name) \
  op_name_to_factory[op_name] = &(opFactory<class_name>);
Op* CreateOp(std::string op_name, std::string instance_name, int parallelism) { 
  if(op_name_to_factory.find(op_name) == op_name_to_factory.end()) {
    fatal_error("No definition for: " + op_name);
  }
  return op_name_to_factory[op_name](instance_name, parallelism);
}

#include "op_identity.hpp"
#include "op_cwise_unary.hpp"
#include "op_cwise_binary.hpp"
#include "op_matmul.hpp"
#include "op_bias_add.hpp"
#include "op_argmax.hpp"
#include "op_reduce_mean.hpp"
#include "op_softmax.hpp"
#include "op_gather.hpp"
#include "op_split.hpp"
#include "op_squeeze.hpp"
#include "op_concat.hpp"
#include "op_reshape.hpp"
#include "op_unpack.hpp"
#include "op_shape.hpp"
#include "op_conv2d.hpp"
#include "op_cast.hpp"
#include "op_maxpool.hpp"
#include "op_avgpool.hpp"
#include "op_batchnorm.hpp"
#include "op_fusedbatchnorm.hpp"

void RegisterOps() {
	RegisterOp("MatMul", MatMulOp);
	RegisterOp("Add", AddOp);
	RegisterOp("Mul", MulOp);
	RegisterOp("Sub", SubOp);
	RegisterOp("Tanh", TanhOp);
	RegisterOp("Sigmoid", SigmoidOp);
	RegisterOp("Rsqrt", RsqrtOp);
	RegisterOp("Relu", ReluOp);
	RegisterOp("BiasAdd", BiasAddOp);
	RegisterOp("ArgMax", ArgMaxOp);
	RegisterOp("Equal", EqualOp);
	RegisterOp("Cast", CastOp);
	RegisterOp("Mean", ReduceMeanOp);
	RegisterOp("Softmax", SoftmaxOp);
	RegisterOp("Gather", GatherOp);
	RegisterOp("Split", SplitOp);
	RegisterOp("SplitV", SplitVOp);
	RegisterOp("Squeeze", SqueezeOp);
	RegisterOp("Concat", ConcatOp);
	RegisterOp("ConcatV2", ConcatOp);
	RegisterOp("Identity", IdentityOp);
	RegisterOp("Reshape", ReshapeOp);
	RegisterOp("Unpack", UnpackOp);
	RegisterOp("Shape", ShapeOp);
  RegisterOp("Conv2D", Conv2DOp);
  RegisterOp("MaxPool", MaxPoolOp);
  RegisterOp("AvgPool", AvgPoolOp);
  RegisterOp("BatchNormWithGlobalNormalization", BatchNormOp);
  RegisterOp("FusedBatchNorm", FusedBatchNormOp);
}

