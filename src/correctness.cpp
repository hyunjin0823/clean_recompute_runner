#include <iostream>
#include <fstream>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <algorithm>
using namespace std;

#include <omp.h>
#include <mkl.h>

#include "graph.h"
#include "load.h"
#include "ops/register.h"

TensorContainer get_tensor(string fname) {
  ifstream infile(fname.c_str());
  if(!infile) fatal_error("Failed to load from " + fname);
  TensorContainer tc = load_tensor(infile);
  if(tc.dtype == DT_NONE) fatal_error("no dtype");
  infile.close();
  return tc;
}

string tensor_fname(const char *prefix, string name, int idx) {
  return string(prefix) + "_canon/" + name + ":" + to_string(idx);
}

template<typename T>
double euclidean(TensorContainer one, TensorContainer two) {
  const T *a = TensorExtractor<T>::Get(one)->ptr();
  const T *b = TensorExtractor<T>::Get(two)->ptr();
  size_t N = TensorExtractor<T>::Get(one)->elements();
  if(N != TensorExtractor<T>::Get(two)->elements()) return 999.0;
  double sum = 0.0;
  for(size_t i = 0; i < N; i++) {
    T diff = a[i] - b[i];
    sum += diff * diff;
  }
  return sqrt(sum);
}

double get_error(TensorContainer one, TensorContainer two) {
  if(one.dtype != two.dtype) return 999.0;
  switch(one.dtype) {
  case DT_FLOAT: return euclidean<float>(one, two);
  case DT_INT: return euclidean<int>(one, two);
  case DT_LONG: return euclidean<long>(one, two);
  default: return -1;
  }
}

int main(int argc, char** argv) {
  RegisterOps();
  if(argc != 2) {
    cerr << "usage: ./test_correctness <graph.def>\n";
    exit(1);
  }

  omp_set_nested(1);
  omp_set_dynamic(0);
  omp_set_max_active_levels(2);
  mkl_set_dynamic(0);

  OpGraph op_graph = load_graph(argv[1]);
  auto names = op_graph.get_op_names();
  sort(names.begin(), names.end());

  double worst_error = -1;
  string worst_error_name;

  for(auto s : names) {
    cout << s;
    std::vector<TensorContainer> feed = op_graph.get_initialized_input(s);
    auto inputs = op_graph.get_input_names(s);
    for(int i = 0; i < inputs.size(); i++) {
      auto p = inputs[i];
      if(p.first.length() > 0) {
        feed[i] = get_tensor(tensor_fname(argv[1], p.first, p.second));
      }
    }
    auto outputs = op_graph.get_op(s).TimedCompute(feed);
    for(int i = 0; i < outputs.size(); i++) {
      double error = get_error(get_tensor(tensor_fname(argv[1], s, i)), outputs[i]);
      if(error > worst_error) {
        worst_error = error;
        worst_error_name = s;
      }
      if(i) cout << "--";
      cout << "\t" << i << '\t' << error << '\n';
    }
  }

  cout << worst_error_name << "\tWORST\t" << worst_error << "\n";

  return 0;
}
