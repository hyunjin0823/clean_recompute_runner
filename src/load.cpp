#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <set>
#include <cstring>

#include "load.h"

bool update_state(std::string line, LoadState &state) {
  if("INPUT" == line) state = INPUT;
  else if("CONST" == line) state = CONST_INPUT;
  else if("OP" == line) state = OP;
  else if("INPUT_FEED" == line) state = INPUT_FEED;
  else if("EDGE" == line) state = EDGE;
  else return false;
  return true;
}

std::unordered_map<std::string, int> get_mapping() {
  std::unordered_map<std::string, int> mapping;
  std::ifstream infile("params/intra.txt");
  if(!infile) fatal_error("Cannot open intra");
  std::string line, op_name;
  int val;
  while(std::getline(infile, line)) {
    if(line.length() == 0) continue;
    if(line[0] == '#') continue;
    std::istringstream iss(line);
    if(iss >> op_name >> val) {
      mapping[op_name] = val;
    } else {
      fatal_error("Formatting error in intra");
    }
  }
  if(mapping.find("default") == mapping.end()) {
    fatal_error("No default case in intra");
  }
  return mapping;
}

int get_parallelism(std::string op_name) {
  static auto mapping = get_mapping();
  if(mapping.find(op_name) == mapping.end()) return mapping["default"];
  return mapping[op_name];
}

void add_control(OpGraph &op_graph) {
  std::ifstream infile("params/control.txt");
  if(!infile) fatal_error("Cannot open control");
  std::string line, op1, op2;
  while(std::getline(infile, line)) {
    if(line.length() == 0) continue;
    if(line[0] == '#') continue;
    std::istringstream iss(line);
    if(iss >> op1 >> op2) {
      op_graph.add_control_edge(op1, op2);
    } else {
      fatal_error("Formatting error in control");
    }
  }
}

template<typename T>
void fill_from_stream(std::istream &source, T *ptr, size_t num) {
  for(size_t i = 0; i < num; i++) {
    source >> ptr[i];
  }
}

/*template<>
void fill_from_stream(std::istream &source, float *ptr, size_t num) {
  for(size_t i = 0; i < num; i++) {
    source >> (ptr[i]);
    ptr[i] *= 100;
  }
}*/

template<>
void fill_from_stream(std::istream &source, std::string *ptr, size_t num) {
  for(size_t i = 0; i < num; i++) {
    source >> std::quoted(ptr[i]);
  }
}

TensorContainer set_batching(std::string model_name, std::string tensor_name, TensorContainer in) {
  static bool init = true;
  static int model_batch, set_batch;
  static std::set<std::string> batch_ops;
  if(init) {
    std::ifstream infile((model_name + "_batch.txt").c_str());
    infile >> model_batch;
    std::string in_op;
    while(infile >> in_op) batch_ops.insert(in_op);
    infile.close();
    init = false;
    std::ifstream infile2("params/batch.txt");
    infile2 >> set_batch;
    infile2.close();
  }
  if(model_batch == set_batch) return in;
  if(batch_ops.find(tensor_name) != batch_ops.end()) {
    if(in.shape()[0] != model_batch) fatal_error("dimension");
    auto out_shape = in.shape();
    out_shape[0] = set_batch;
    TensorContainer out(in.dtype, out_shape);
    size_t stride = in.elements() / model_batch;
    switch(in.dtype) {
    #define make_case(E_DTYPE, R_DTYPE) case E_DTYPE: \
      for(int i = 0; i < set_batch; i++) { \
        memcpy(TensorExtractor<R_DTYPE>::Get(out)->ptrInit() + i * stride, \
               TensorExtractor<R_DTYPE>::Get(in)->ptr(), \
               stride * sizeof(R_DTYPE)); \
      } \
      break;
    make_case(DT_FLOAT, float)
    make_case(DT_INT, int)
    make_case(DT_LONG, long)
    #undef make_case
    default: fatal_error("dtype");
    }
    return out;
  }
  return in;
}

TensorContainer load_tensor(std::istream &infile) {
  if(!infile) {
    std::cerr << "Could not load variable from stream\n";
    return TensorContainer();
  }
  std::string dtype_s;
  infile >> dtype_s;
  size_t ndims;
  infile >> ndims;
  if(0 == ndims) {
    #define make_case(str, dtype) if(dtype_s == str) { \
      dtype val; \
      fill_from_stream(infile, &val, 1); \
      return TensorContainer(val); \
    }  
    make_case("DT_FLOAT", float);
    make_case("DT_INT", int);
    make_case("DT_LONG", long);
    make_case("DT_STRING", std::string);
    #undef make_case
    std::cerr << "Unknown tensor dtype: " << dtype_s << '\n';
    return TensorContainer();
  }
  else {
    std::vector<size_t> dims(ndims);
    for(size_t i = 0; i < ndims; i++)
    {
      infile >> dims[i];
    }
    #define make_case(token, token_str, dtype) if(dtype_s == token_str) { \
      TensorContainer tc(token, dims); \
      Tensor<dtype> *tensor = TensorExtractor<dtype>::Get(tc); \
      fill_from_stream(infile, tensor->ptrInit(), tensor->elements()); \
      return tc; \
    }
    make_case(DT_FLOAT, "DT_FLOAT", float);
    make_case(DT_INT, "DT_INT", int);
    make_case(DT_LONG, "DT_LONG", long);
    make_case(DT_STRING, "DT_STRING", std::string);
    #undef make_case
    std::cerr << "Unknown tensor dtype: " << dtype_s << '\n';
    return TensorContainer();
  }
}

void change_graph_input(OpGraph &opGraph, std::string model_name, std::string infile){

  std::ifstream t_infile;
  std::string input_file_name;
#if !defined(NDEBUG)
  printf("loading input file %s\n", infile.c_str());
#endif
  opGraph.reset();
  input_file_name =  model_name + "_vars/" + infile + ".var";
#if !defined(NDEBUG)
  printf("input file_name is set to %s\n", input_file_name.c_str());
#endif
  t_infile.open(input_file_name.c_str());
  TensorContainer input_tensor = set_batching(model_name, "ResizeBilinear", load_tensor(t_infile));
  opGraph.change_input("Sub", 0, input_tensor);
  t_infile.close();

}

OpGraph load_graph(std::string model_name) {
  std::ifstream infile((model_name + ".def").c_str());
  if(!infile) fatal_error("Bad filename: " + model_name + ".def");
  OpGraph op_graph;
  std::unordered_map<std::string, TensorContainer> matrices;
  std::string line, str1, str2;
  unsigned idx, idx2;
  LoadState state = NONE;
  while(getline(infile, line)) {
    if(update_state(line, state)) continue;
    std::istringstream iss(line);
    std::ifstream t_infile;
    switch(state) {
    case INPUT:
      iss >> str1;
      if(!iss.eof() || iss.bad()) fatal_error("Malformed graph file line: " + line);
      //std::cout << "input: " << str1 << '\n';
      str2 = model_name + "_vars/" + str1 + ".var";
      t_infile.open(str2.c_str());
      matrices.insert(make_pair(str1, set_batching(model_name, str1, load_tensor(t_infile))));
      t_infile.close();
      if(matrices[str1].dtype == DT_NONE) fatal_error("Failure loading: " + str1);
      break;
    case CONST_INPUT:
      iss >> str1;
      //std::cout << "const: " << str1 << '\n';
      matrices.insert(make_pair(str1, set_batching(model_name, str1, load_tensor(iss))));
      if(iss.bad()) fatal_error("Malformed graph file line: " + line);
      if(matrices[str1].dtype == DT_NONE) fatal_error("Failure loading (inline): " + str1);
      break;
    case OP:
      iss >> str1 >> str2;
      if(!iss.eof() || iss.bad()) fatal_error("Malformed graph file line: " + line);
      //std::cout << "op: " << str1 << ": " << str2 << '\n';
      op_graph.add_op(str1, str2, get_parallelism(str2));
      break;
    case INPUT_FEED:
      iss >> str1 >> idx >> str2;
      if(!iss.eof() || iss.bad()) fatal_error("Malformed graph file line: " + line);
      //std::cout << "input: " << str2 << " -> " << str1 << '\n';
      if(matrices.find(str2) == matrices.end()) {
        fatal_error("matrix " + str2 + " unavailable");
      }
      op_graph.add_input(str1, idx, matrices[str2]);
      break;
    case EDGE:
      iss >> str1 >> idx >> str2 >> idx2;
      if(!iss.eof() || iss.bad()) fatal_error("Malformed graph file line: " + line);
      //std::cout << "edge: " << str2 << " -> " << str1 << '\n';
      op_graph.add_data_edge(str2, str1, idx, idx2);
      break;
    case NONE:
      fatal_error("unqualified line in def file");
      break;
    }
  }

  add_control(op_graph);
  op_graph.prepare();
  return op_graph;
}

