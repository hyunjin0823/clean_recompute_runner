#ifdef USE_CUDA

#include <memory>
#include <cassert>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdlib.h>
#include <stdio.h>

#include "def.h"

template<typename T>
Tensor<T>::Tensor(const std::vector<size_t> &in_dimensions) : 
      dimensions(in_dimensions),
      scalar_val(0),
      h_state(UNALLOCATED),
      d_state(UNALLOCATED) {
  num_elements = elements_count(in_dimensions);
}

template<typename T>
Tensor<T>::Tensor(const T val) :
      dimensions(0),
      scalar_val(val),
      num_elements(0),
      h_state(UNALLOCATED),
      d_state(UNALLOCATED) {}

template<typename T>
Tensor<T>::~Tensor() {
    std::free(h_data);
    cudaFree(d_data);
}

template<typename T>
T *Tensor<T>::ptrInit(TargetDevice dev, cudaStream_t * stream) {
  assert(dimensions.size() > 0);
  assert(dimensions[0] > 0);
  switch(dev) {
    case CPU:
      switch(h_state) {
        case UNALLOCATED:
          h_data = (T *)malloc(num_elements * sizeof(T));
        case INVALID:
          if (d_state == RESIDENT) {
            cudaMemcpy((void *)h_data, (void *)d_data, num_elements * sizeof(T), cudaMemcpyDeviceToHost);
            cudaDeviceSynchronize();
            d_state = INVALID;
          }
        case RESIDENT:
          h_state = RESIDENT;
          return h_data;
        default:
          fatal_error("INVALID HOST MEMORY TYPE");
      }
    case GPU:
      switch (d_state) {
        case UNALLOCATED:
          cudaMalloc((void **)&d_data, num_elements * sizeof(T));
        case INVALID:
          if (h_state == RESIDENT) {
            cudaMemcpy((void *)d_data, (void *)h_data, num_elements * sizeof(T), cudaMemcpyHostToDevice);
            cudaDeviceSynchronize();
            h_state = INVALID;
          }
        case RESIDENT:
          d_state = RESIDENT;
          return d_data;
        default:
          fatal_error("INVALID DEVICE MEMORY STATE");
      }
    default:
      fatal_error("INVALID DEVICE TYPE");
  }
}

template<typename T>
const T *Tensor<T>::ptrShr(TargetDevice dev, cudaStream_t * stream)  {
  assert(dimensions.size() > 0);
  assert(dimensions[0] > 0);
 
  if (!(d_state == RESIDENT || h_state == RESIDENT)) fatal_error("FOR CONST ACCESS MUST BE RESIDENT IN MEMORY");

  switch(dev) {
    case CPU:
      switch(h_state) {
        case UNALLOCATED:
          h_data = (T *)malloc(num_elements * sizeof(T));
        case INVALID:
          cudaMemcpy((void *)h_data, (void *)d_data, num_elements * sizeof(T), cudaMemcpyDeviceToHost);
          cudaDeviceSynchronize();
        case RESIDENT:
          h_state = RESIDENT;
          return h_data;
        default:
          fatal_error("INVALID HOST MEMORY TYPE");
       }
    case GPU:
      std::cout << "GPU Pointer requested\n";
      switch (d_state) {
        case UNALLOCATED:
          std::cout << "Data state unallocated\n";
          break;
        case INVALID:
          std::cout << "Data state unallocated\n";
          break;
        case RESIDENT:
          std::cout << "Data state unallocated\n";
          break;
      }
      switch (d_state) {
        case UNALLOCATED:
          std::cout << "Allocating...";
          cudaMalloc((void **)&d_data, num_elements * sizeof(T));
        case INVALID:
          std::cout << "Copying....";
          cudaMemcpy((void *)d_data, (void *)h_data, num_elements * sizeof(T), cudaMemcpyHostToDevice);
          cudaDeviceSynchronize();
        case RESIDENT:
          std::cout << "Done\n";
          std::cout << "Pointer value: " << d_data << "\tNum Elements: " << num_elements << "\n";
          d_state = RESIDENT;
          return d_data;
        default:
          fatal_error("INVALID DEVICE MEMORY STATE");
      }
    default:
      fatal_error("INVALID DEVICE TYPE");
  }
}

template<typename T>
const T *Tensor<T>::ptr(TargetDevice dev, cudaStream_t * stream) {
  return ptrShr(dev, stream);
}


#endif
